import React from 'react';
import './App.css';

import {
  BrowserRouter as Router,
  Switch,
  Route, Redirect
} from "react-router-dom";

import Nav from './components/Nav/Nav';
import UsersPage from './pages/UsersPage';
import SettingsPage from './pages/ActionsPage';
import PaymentsPage from './pages/PaymentsPage';


function App() {
  return (
    <Router>
      <Nav />
      <Switch>
        <Redirect exact from="/" to="/home" />
        <Route exact path="/payment">
          <PaymentsPage />
        </Route>
        <Route exact path="/home">
          <UsersPage />
        </Route>
        <Route exact path="/actions">
          <SettingsPage />
        </Route>
      </Switch>
    </Router>

  );
}

export default App;
