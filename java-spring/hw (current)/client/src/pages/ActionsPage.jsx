import React from "react";
import CustomersActionForm from "../components/Forms/ActionsForms/CustomersActionForm";
import AccountsActionForm from "../components/Forms/ActionsForms/AccountsActionForm";

function ActionsPage() {
    return (<div className="page">
        <p className='heading'>ACTIONS PAGE</p>
        <CustomersActionForm type="edit" />
        <CustomersActionForm type="delete" />
        <AccountsActionForm type="open" />
        <AccountsActionForm type="close" />
    </div>)
}
export default ActionsPage;