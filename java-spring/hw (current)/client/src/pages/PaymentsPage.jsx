import React from "react";
import PaymentsForm from "../components/Forms/PaymentsForm/PaymentsForm";


function PaymentsPage() {
    return (<div className="page">
        <p className='heading'>PAYMENTS PAGE</p>
        <PaymentsForm type="deposit" />
        <PaymentsForm type="withdraw" />
        <PaymentsForm type="transfer" />
    </div>)
}
export default PaymentsPage;