import React from "react";

import Users from "../components/Users/Users";

function UsersPage() {
    return (<div className="page">
        <p className='heading'>CUSTOMERS PAGE</p>
        <Users></Users>
    </div>)
}

export default UsersPage;