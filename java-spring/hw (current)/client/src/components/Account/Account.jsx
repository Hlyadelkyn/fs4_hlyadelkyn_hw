import React from "react";

import "./Account.css";

function Account(props) {
    return (
        <li className="account">
            <p className="account__id"     >{props.id}</p>
            <p className="account__number" >{props.number}</p>
            <p className="account__balance">{props.balance} {props.currency}</p>

        </li>
    )
}
export default Account;