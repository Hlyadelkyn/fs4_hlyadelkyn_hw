import React from "react";
import { NavLink } from "react-router-dom";

import "./Nav.css"

function Nav() {
    return (<>
        <ul className="navigation">
            <li className="navigation__el">
                <NavLink to="/actions">actions</NavLink>
            </li>
            <li className="navigation__el" >
                <NavLink to="/payment">payment</NavLink>
            </li>
            <li className="navigation__el" >
                <NavLink to="/home">home</NavLink>
            </li>
        </ul>
    </>)
}

export default Nav;