import React from "react";
import { Formik, Field, Form } from 'formik';

export default function CustomersActionForm(props) {
    return (<div className="paymentform">
        <p className="paymentform__heading">{props.type} customer</p>
        <Formik
            initialValues={{
                uid: '',
                toname: '',
                toemail: '',
                toage: ''
            }}
            onSubmit={async (values) => {
                console.log(props.type);
                console.log(values);

                let action = props.type === "edit" ? "edit" : "delete";
                let params = props.type === "edit" ? "?" + new URLSearchParams({
                    name: values.toname,
                    email: values.toemail,
                    age: values.toage,
                }) : "";


                let URI = props.type === "edit" ? "http://localhost:9000/users/" + values.uid + "/" + action + params : "http://localhost:9000/users/" + action + "/" + values.uid
                console.log(URI);
                async function fetchData() {
                    let method = props.type === "edit" ? "PUT" : "DELETE";
                    try {
                        const response = await fetch(URI, {
                            method: method, // Replace with the appropriate HTTP method
                            headers: {
                                'Origin': 'http://localhost:8080', // Replace with the URL of your React app
                            },
                        });
                        const json = await response.json();
                        console.log(json);
                    } catch (error) {
                        console.log("error", error);
                    }
                }
                fetchData();
            }}
        >
            <Form>
                <label className="paymentsform__label" htmlFor="amount">user id</label>
                <Field
                    className="paymentsform__field"
                    id="uid"
                    name="uid"
                    placeholder={"user indentification number"}
                />

                {props.type === "delete" ? <></> : <>
                    <label className="paymentsform__label" htmlFor="toname">change name to</label>
                    <Field className="paymentsform__field" id="toname" name="toname" placeholder="type value change name to" />

                    <label className="paymentsform__label" htmlFor="toemail">change email to</label>
                    <Field className="paymentsform__field" id="toemail" name="toemail" placeholder="type value change email to" />

                    <label className="paymentsform__label" htmlFor="toage">change age to</label>
                    <Field className="paymentsform__field" id="toage" name="toage" placeholder="type value change age to" />
                </>}




                <button className="paymentsform__sbm" type="submit">Submit</button>
            </Form>
        </Formik>

    </div>)
}