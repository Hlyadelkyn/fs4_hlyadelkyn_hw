import React from "react";
import { Formik, Field, Form } from 'formik';

export default function AccountsActionForm(props) {
    return (<div className="act-form">
        <p className="paymentform__heading">{props.type} account</p>
        <Formik
            initialValues={{
                uid: '',
                currency: '',
                aid: ''
            }}
            onSubmit={async (values) => {
                console.log(props.type);
                console.log(values);
                let actiontype = props.type === "open" ? "create-account?" + new URLSearchParams({
                    currency: values.currency,
                }) : "delete-account?" + new URLSearchParams({
                    accountId: values.aid,
                });
                let URI = "http://localhost:9000/users/" + values.uid + "/" + actiontype;

                async function fetchData() {
                    let method = props.type === "open" ? "POST" : "DELETE";
                    console.log(URI + " " + method);
                    try {
                        const response = await fetch(URI, {
                            method: method, // Replace with the appropriate HTTP method
                            headers: {
                                'Origin': 'http://localhost:8080', // Replace with the URL of your React app
                            },
                        });
                        const json = await response.json();
                        console.log(json);
                    } catch (error) {
                        console.log("error", error);
                    }
                }
                fetchData();
            }}
        >
            <Form>
                <label className="paymentsform__label" htmlFor="uid">user id</label>
                <Field
                    className="paymentsform__field"
                    id="uid"
                    name="uid"
                    placeholder={"user indentification number"}
                />

                {props.type === "close" ? <>
                    <label className="paymentsform__label" htmlFor="uid">account id</label>
                    <Field
                        className="paymentsform__field"
                        id="aid"
                        name="aid"
                        placeholder={"account indentification number"}
                    />
                </> : <>
                    <label className="paymentsform__label" htmlFor="aid">account currency</label>
                    <Field children className="paymentsform__field" as="select" id="currency" name="currency" placeholder="currency" >
                        <option value={"uah"} key={"uah"}>uah</option>
                        <option value={"usd"} key={"usd"}>usd</option>
                        <option value={"eur"} key={"eur"}>eur</option>
                        <option value={"chf"} key={"chf"}>chf</option>
                        <option value={"gbp"} key={"gbp"}>gbp</option>
                    </Field>
                </>}




                <button className="paymentsform__sbm" type="submit">Submit</button>
            </Form>
        </Formik>

    </div>)
}
//http://localhost:9000/users/3/create-account?currency=usd.
//http://localhost:9000/users/3/create-account?currency=usd.