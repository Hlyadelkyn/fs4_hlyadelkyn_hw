import React from "react";
import { Formik, Field, Form } from 'formik';

import "../PaymentsForm.css"

export default function PaymentsForm(props) {
    return (<div className="paymentform">
        <p className="paymentform__heading">{props.type}</p>
        <Formik
            initialValues={{
                source_number: '',
                target_number: '',
                amount: '',
            }}
            onSubmit={async (values) => {
                console.log(props.type);
                console.log(values);
                let URI = "http://localhost:9000/accounts/transfer?";
                if (props.type === "deposit") {
                    URI = "http://localhost:9000/accounts/deposit?";
                } else if (props.type === "withdraw") {
                    URI = "http://localhost:9000/accounts/withdraw?";
                }

                async function fetchData() {
                    try {
                        console.log(URI + new URLSearchParams({
                            source: values.source_number,
                            amount: values.amount,
                            target: values.target_number,
                        }));
                        const response = await fetch(URI + new URLSearchParams({
                            source: values.source_number,
                            amount: values.amount,
                            target: values.target_number,
                        }), {
                            method: 'PUT', // Replace with the appropriate HTTP method
                            headers: {
                                'Origin': 'http://localhost:8080', // Replace with the URL of your React app
                            },
                        });
                        const json = await response.json();
                        console.log(json);
                    } catch (error) {
                        console.log("error", error);
                    }
                }
                fetchData();
            }}
        >
            <Form>

                {props.type === "deposit" ? <><label className="paymentsform__label" htmlFor="target_number">Target account number</label>
                    <Field className="paymentsform__field" id="target_number" name="target_number" placeholder="xxxx-xxxx-xxxx" /></> : <><label className="paymentsform__label" htmlFor="source_number">Source account number</label>
                    <Field className="paymentsform__field" id="source_number" name="source_number" placeholder="xxxx-xxxx-xxxx" /></>}



                {props.type === "transfer" ? <><label className="paymentsform__label" htmlFor="target_number">Target account number </label>
                    <Field id="target_number" className="paymentsform__field" name="target_number" placeholder="xxxx-xxxx-xxxx" /> </> : <></>}



                <label className="paymentsform__label" htmlFor="amount">Amount</label>
                <Field
                    className="paymentsform__field"
                    id="amount"
                    name="amount"
                    placeholder={"amount " + props.type + " to"}
                />

                <button className="paymentsform__sbm" type="submit">Submit</button>
            </Form>
        </Formik>
    </div>)
} 