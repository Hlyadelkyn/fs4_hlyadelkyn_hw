import React from "react";
import "./User.css";


import Account from "../Account/Account";

function User(props) {
    return (
        <li className="user">
            <div className="user__properties">
                <p className="user__id"    >{props.id}</p>
                <p className="user__name"  >{props.name}</p>
                <p className="user__email" >{props.email}</p>
                <p className="user__age"   >{props.age}</p>
            </div>


            {props.withAccounts === true ?
                <>
                    {
                        props.accounts.length !== 0 ? <>
                            <p className="accounts-label">Accounts:</p>
                            <ul className='accounts'>
                                <Account number="number" id="id" balance="balance" currency="" />
                                {props.accounts.map(({ number, id, balance, currency }) => {
                                    return <Account number={number} key={id} id={id} balance={balance} currency={currency} />
                                })}
                            </ul></> : <p className="accounts-label">No opened accounts yet</p>}</>
                : <></>}

        </li>
    )
}
export default User;