import React, { useState, useEffect } from 'react';
import "./Users.css";

import User from '../User/User';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';

const URI = "http://localhost:9000/users?page=1";

function Users() {
    const [users, setUsers] = useState([]);

    let history = useHistory();

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(URI, {
                    method: 'GET',
                    headers: {
                        'Origin': 'http://localhost:8080', // Replace with the URL of your React app
                    },
                });
                console.log(response);
                if (response.status === 401) { // Unauthorized status
                    history.push('/login'); // Redirect to the login page
                    return;
                }
                const json = await response.json();
                setUsers(json);
            } catch (error) {
                console.log("error", error);

            }
        }
        fetchData();
    }, []);

    return (
        <ul className='users'>
            <User name="name" id="id" email="email" age="age" withAccounts={false} />
            {users.map(({ name, id, email, age, accounts }) => {
                return <User name={name} id={id} email={email} age={age} key={id} accounts={accounts} withAccounts={true} />
            })}
        </ul>
    )
}
export default Users;