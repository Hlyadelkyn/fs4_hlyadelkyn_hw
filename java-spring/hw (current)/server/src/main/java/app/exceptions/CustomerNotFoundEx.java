package app.exceptions;

import lombok.Getter;

@Getter
public class CustomerNotFoundEx extends RuntimeException{
    private Long id;

    public CustomerNotFoundEx(final long i){
        super("Customer with id " + i + " not found");
        this.id = i;
    }
}
