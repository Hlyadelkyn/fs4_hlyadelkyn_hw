package app.exceptions;

import lombok.Getter;

@Getter
public class AccountNotFoundEx extends RuntimeException{
    private Long id;

    public AccountNotFoundEx(final long i){
        super("Account with id " + i + " not found");
        this.id = i;
    }
}
