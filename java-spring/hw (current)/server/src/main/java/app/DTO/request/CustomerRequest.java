package app.DTO.request;

import app.utils.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@NoArgsConstructor
public class CustomerRequest {
    @NotEmpty
    @Size(min = 2,   message = "user name must have at least 2 characters")
    private String  name;

    public CustomerRequest(String n, String e, Integer a, String p, String pn){
        name = n;
        email = e;
        age = a;
        password = p;
        phoneNumber = pn;
    }
    @NotEmpty
    @Email
    private String email;

    @Min(value = 18, message = "user age must be at least 18")
    private Integer age;

    @ValidPassword
    @NotEmpty
    private String  password;

    @NotEmpty
    @Pattern(regexp = "^(\\+38)?\\s?(050|063|066|067|068|091|092|093|094|095|096|097|098|099)\\s?\\d{3}\\s?\\d{2}\\s?\\d{2}$",
            message = "invalid ukrainian phone number")
    private String  phoneNumber;
}
