package app.DTO.response;

import app.model.Account;
import app.model.Employer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class DetailedCustomerResponse {
    int                           id;
    String                        name;
    String                        email;
    int                           age;
    List<Account>                 accounts;
    List<PublicEmployerResponse>  employers;
    String                        phoneNumber;

    public DetailedCustomerResponse(int i, String n, String e,  int a , List<Account> al, List<PublicEmployerResponse> el, String phn){
        id           = i;
        name         = n;
        email        = e;
        age          = a;
        accounts     = al;
        employers    = el;
        phoneNumber  = phn;
    }
}
