package app.DTO.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployerRequest {
    @NotEmpty
    @Size(min = 2,   message = "employer name must have at least 2 characters")
    String name;

    @NotEmpty
    @Size(min = 3,   message = "emps address name must have at least 3 characters")
    String address;
}
