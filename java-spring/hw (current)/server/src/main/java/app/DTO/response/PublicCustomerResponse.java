package app.DTO.response;

import app.model.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
public class PublicCustomerResponse {
    int             id;
    String          name;
    List<Account>   accounts;
    public PublicCustomerResponse(int i, String n, List<Account>a){
        id       = i;
        name     = n;
        accounts = a;
    }
}
