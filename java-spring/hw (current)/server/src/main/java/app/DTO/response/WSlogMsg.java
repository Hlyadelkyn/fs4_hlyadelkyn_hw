package app.DTO.response;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class WSlogMsg {
    String message;
    LocalDateTime time;

    public WSlogMsg(String m, LocalDateTime t){
        message = m;
        time = t;
    }

}
