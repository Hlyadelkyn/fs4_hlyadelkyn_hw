package app.DTO.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TransferResponse {
    private AccountResponse source;
    private AccountResponse target;
    private double amount;

    public TransferResponse(AccountResponse s, AccountResponse t, double a){
        source = s;
        target = t;
        amount = a;
    }
}
