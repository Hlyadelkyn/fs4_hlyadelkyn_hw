package app.DTO.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginResponse {
    private Boolean status;
    private String error;
    private String token;

    public static LoginResponse Ok(String token) {
        return new LoginResponse(true, null, token);
    }

    public static LoginResponse Error(String message) {

        return new LoginResponse(false, message, null);
    }

}
