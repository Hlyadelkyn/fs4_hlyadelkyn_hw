package app.DTO.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TransferRequest {
    private AccountRequest source;
    private AccountRequest target;
    private double amount;
    public  TransferRequest(AccountRequest s, AccountRequest t, double a){ source=s; target=t;amount=a;}
}
