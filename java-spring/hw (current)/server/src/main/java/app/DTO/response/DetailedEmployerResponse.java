package app.DTO.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetailedEmployerResponse {
    long id;
    String name;
    String address;
    List<PublicCustomerResponse> customers;
}
