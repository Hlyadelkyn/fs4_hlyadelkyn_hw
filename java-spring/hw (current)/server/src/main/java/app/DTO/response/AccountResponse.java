package app.DTO.response;

import app.model.Currency;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountResponse {
    private Integer   id;
    private String    number;
    private double    balance;
    private Currency  currency;
}
