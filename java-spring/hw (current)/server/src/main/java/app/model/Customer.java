package app.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@NoArgsConstructor
@Setter
@Getter
public class Customer  extends  AbstractEntity implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long           id;

    String         name;
    String         email;
    Integer        age;
    @JsonManagedReference
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Account>  accounts;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Authority> authorities;

    private String password;
    String         phoneNumber;


    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "customers")
    @JsonBackReference
    List<Employer> employers;
    public Customer(String n, String e, Integer a){
        name = n;
        email = e;
        age = a;
        accounts = new ArrayList<>();
    }
    public void addAccount(Account a){
        accounts.add(a);
    }
    public void deleteAccount(Account a){
        accounts.remove(a);
    }

    @Override
    public String toString() {
        int n = 20 - name.length();
        int m = 25 - email.length();
        return String.format("customer ID: %S | customer name : %S %S| customer e-mail : %S %S| customer age : %S", id, name, " ".repeat(n), email, " ".repeat(m), age);
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }






    @Override
    public String getUsername() {
        return name;
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
}
