package app.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.text.DecimalFormat;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String number;
    Currency currency;
    double balance;
    @JsonBackReference
    @ManyToOne
    Customer customer;
    public Account(Currency cur, Customer cus){
        this.currency = cur;
        this.customer = cus;
        this.number = UUID.randomUUID().toString();
        this.balance = 0;
    }
    public Account(Currency cur){
        this.currency = cur;
        this.number = UUID.randomUUID().toString();
        this.balance = 0;
    }
    public void deposit(double a){
        this.balance += a;
    }
    public void withdraw(double a){
        this.balance = this.balance - a;
    }
    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#.##");
        String s = " ".repeat(7 - String.valueOf(df.format(balance)).length());
        return String.format("account  ID: %S | account number : %S | account balance : %S %S %S || customer details | CUSTOMER ID : %S |", id, number, df.format(balance), currency , s,  customer.getId());
    }
}
