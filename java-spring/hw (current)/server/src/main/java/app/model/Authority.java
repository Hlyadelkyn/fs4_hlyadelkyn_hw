package app.model;

import app.model.Customer;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Data
@Entity
public class Authority implements GrantedAuthority {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String authority;

    @ManyToOne
    private Customer customer;


    public Authority (String s){
        authority = s;
    }

    public Authority() {}

    @Override
    public String getAuthority() {
        return authority;
    }
}
