package app.security.jwt;

import io.jsonwebtoken.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;


@Log4j2
@Service
@PropertySource("classpath:jwt.properties")
public class JwtService implements Serializable {

    @Value("${jwt.secret}")
    private String secret;

    public static final long JWT_TOKEN_VALIDITY = 10*60*60 * 1000L;
    public String generate(Long userId) {
        Date now = new Date();
        Date expiry = new Date(now.getTime() + JWT_TOKEN_VALIDITY);
        return Jwts.builder()
                .setSubject(userId.toString())
                .setIssuedAt(now)
                .setExpiration(expiry)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    private Optional<Jws<Claims>> parseToClaims(String token) {
        try {
            return Optional.of(Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token));
        } catch (JwtException x) {
            log.error("something went wrong with token", x);
            return Optional.empty();
        }
    }

    public Optional<Integer> parse(String token) {
        return parseToClaims(token)
                .map(Jwt::getBody)
                .map(Claims::getSubject)
                .map(Integer::parseInt);
    }

}
