package app.security.jwt;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class JwtFilter extends OncePerRequestFilter {
    private static final String bearer = "Bearer ";

    private static final String header = HttpHeaders.AUTHORIZATION;

    private final JwtService serv;
    private Optional<String> extract(HttpServletRequest req) {
        return Optional.ofNullable(req.getHeader(header))
                .filter(token -> token.startsWith(bearer))
                .map(token -> token.substring(bearer.length()));
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws ServletException, IOException {
        extract(req)
                .flatMap(serv::parse)
                .map(JwtUD::new)
                .map(details -> new UsernamePasswordAuthenticationToken(details, null, details.getAuthorities()))
                .ifPresentOrElse(at -> {
                    at.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
                    SecurityContextHolder.getContext().setAuthentication(at);
                }, () -> res.setStatus(403));
        chain.doFilter(req, res);
    }
}
