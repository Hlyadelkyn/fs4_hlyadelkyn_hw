package app.security;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class CustomPasswordEncoder  {
    private PasswordEncoder passwordEncoder;

    public CustomPasswordEncoder(){
        passwordEncoder = new Pbkdf2PasswordEncoder();
    }
    public PasswordEncoder getPasswordEncoder(){
        return  passwordEncoder;
    }
}
