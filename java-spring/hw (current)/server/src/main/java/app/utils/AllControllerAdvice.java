package app.utils;

import app.exceptions.AccountNotFoundEx;
import app.exceptions.CustomerNotFoundEx;
import org.springframework.hateoas.mediatype.vnderrors.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Optional;

@ControllerAdvice
public class AllControllerAdvice    {
    @ExceptionHandler(CustomerNotFoundEx.class)
    public ResponseEntity<VndErrors> customerNotFoundException(final CustomerNotFoundEx e) {
        return error(e, HttpStatus.NOT_FOUND, e.getId().toString());
    }
    @ExceptionHandler(AccountNotFoundEx.class)
    public ResponseEntity<VndErrors> accountNotFoundException(final  AccountNotFoundEx e) {
        return error(e, HttpStatus.NOT_FOUND, e.getId().toString());
    }

    private ResponseEntity<VndErrors> error(
            final Exception exception, final HttpStatus httpStatus, final String logRef) {
        final String message =
                Optional.of(exception.getMessage()).orElse(exception.getClass().getSimpleName());
        return new ResponseEntity<>(new VndErrors(logRef, message), httpStatus);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<VndErrors> assertionException(final IllegalArgumentException e) {
        return error(e, HttpStatus.NOT_FOUND, e.getLocalizedMessage());
    }
}
