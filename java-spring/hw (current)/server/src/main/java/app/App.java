package app;

import app.config.YAMLConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaAuditing
public class App implements CommandLineRunner {
    @Autowired
    YAMLConfig config;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(App.class);
        app.run();
    }
    public void run(String... args) throws Exception {
        System.out.println("\n using environment: " + config.getEnvironment());
    }
}
