package app.config;

import app.security.jwt.JwtFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@EnableWebSecurity @RequiredArgsConstructor
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private final JwtFilter              jwtFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http    .csrf().disable();

        http    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http
                .authorizeRequests()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/auth/**").permitAll()
                .antMatchers("/users/**").authenticated()
                .antMatchers("/users**").authenticated()
                .antMatchers("/emp/**").authenticated()
                .antMatchers("/emp**").authenticated()
                .antMatchers("/accounts/**").authenticated();

        http    .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);

        http    .formLogin().permitAll();

    }

}
