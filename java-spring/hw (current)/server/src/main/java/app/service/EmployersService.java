package app.service;

import app.DTO.request.EmployerRequest;
import app.DTO.response.DetailedEmployerResponse;
import app.mappers.Facade;
import app.model.Customer;
import app.model.Employer;
import app.repo.CustomersRepo;
import app.repo.EmployersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployersService {
    @Autowired
    private EmployersRepo eRepo;
    @Autowired
    private CustomersRepo cRepo;

    @Autowired
    private Facade mapper;
    public List<DetailedEmployerResponse> getAll(){
        return eRepo.findAll().stream().map(employer -> mapper.employerToDTO(employer)).toList();
    }
    public DetailedEmployerResponse       findById(long id){
        return mapper.employerToDTO(eRepo.findById(id).get());
    }
    public DetailedEmployerResponse       create(EmployerRequest req){
        Employer e = new Employer(req.getName(), req.getAddress());
        Employer resp = eRepo.save(e);
        return mapper.employerToDTO(resp);
    }
    public DetailedEmployerResponse       link(long eid, long cid){
        Employer emp = eRepo.findById(eid).get();
        Customer cus = cRepo.findById(cid).get();

        emp.addCustomer(cus);

        Employer resp = eRepo.save(emp);
        return mapper.employerToDTO(resp);
    }
    public DetailedEmployerResponse       unlink(long eid, long cid){
        Employer emp = eRepo.findById(eid).get();
        Customer cus = cRepo.findById(cid).get();

        emp.removeCustomer(cus);

        Employer resp = eRepo.save(emp);
        return mapper.employerToDTO(resp);
    }
}
