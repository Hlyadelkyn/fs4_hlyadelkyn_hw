package app.service;

import app.DTO.request.AccountRequest;
import app.DTO.response.AccountResponse;
import app.DTO.request.TransferRequest;
import app.DTO.response.TransferResponse;
import app.exceptions.AccountNotFoundEx;
import app.mappers.Facade;
import app.model.Account;
import app.model.Customer;
import app.repo.AccountsRepo;
import app.repo.CustomersRepo;
import app.model.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class AccountsService {
    private CustomersRepo customerRepo;
    private AccountsRepo  accountsRepo;
    private Facade        mapper;

    @Autowired
    AccountsService(CustomersRepo cr, AccountsRepo  ar, Facade m){
        accountsRepo = ar;
        customerRepo = cr;
        mapper       = m ;
    }

    AccountsService(CustomersRepo cr){
        customerRepo = cr;
    }
    AccountsService(AccountsRepo  ar){
        accountsRepo = ar;
    }

    public AccountResponse  createAccount(long id, String currency){
        try {
            Customer c            = customerRepo.findById(id).get();

            Currency currencyEnum = getCurrencyFromString(currency);
            Account  a            = new Account(currencyEnum);

            c.addAccount( a);
            a.setCustomer(c);

            Account finalAcc      = accountsRepo.save(a);
            customerRepo.save(c);
            return mapper.accountToDTO(a);
        }catch (NoSuchElementException ex){
            throw new AccountNotFoundEx(id);
        }
    }
    public Boolean          deleteAccount(long id, AccountRequest req){
        long aid = req.getId();
        try{
            Account  a = accountsRepo.findById(aid).get();
            Customer c = customerRepo.findById(id ).get();

            c.deleteAccount(a);
            accountsRepo.deleteById(aid);
            customerRepo.save(c);
            return true;
        }catch (NoSuchElementException ex){
            throw new AccountNotFoundEx(aid);
        }

    }
    Currency                getCurrencyFromString(String currencyString) {
        for (Currency currency : Currency.values()) {
            if (currency.name().equalsIgnoreCase(currencyString)) {
                return currency;
            }
        }
        return null; // Currency not found
    }

    public AccountResponse  deposit(TransferRequest req){
        String number = req.getTarget().getNumber();
        double amount = req.getAmount();

        Account ac = accountsRepo.findByNumber(number);
        ac.deposit(amount);
        accountsRepo.save(ac);
        System.out.println("ACCOUNT  -| DEPOSIT :: " + ac + " AMOUNT : " +amount);
        return mapper.accountToDTO(ac);
    }               //
    public void             deposit(String number, double amount){
        Account ac = accountsRepo.findByNumber(number);
        ac.deposit(amount);
        accountsRepo.save(ac);
        System.out.println("ACCOUNT  -| DEPOSIT :: " + ac + " AMOUNT : " +amount);
        mapper.accountToDTO(ac);
    }
    public AccountResponse  withdraw(TransferRequest req){
        String number = req.getSource().getNumber();
        double amount = req.getAmount();

        Account ac = accountsRepo.findByNumber(number);
        if(ac.getBalance() > amount){
            ac.withdraw(amount);
            accountsRepo.save(ac);
            System.out.println("ACCOUNT  -| WITHDRAW:: " + ac + " AMOUNT : " +amount);
            return mapper.accountToDTO(ac);
        }
        return null;
    }              //
    public void             withdraw(String number, double amount){
        Account ac = accountsRepo.findByNumber(number);
        if(ac.getBalance() > amount){
            ac.withdraw(amount);
            accountsRepo.save(ac);
            System.out.println("ACCOUNT  -| WITHDRAW:: " + ac + " AMOUNT : " +amount);
            mapper.accountToDTO(ac);
        }
    }
    public TransferResponse transfer(TransferRequest req){
        String s = req.getSource().getNumber().toLowerCase();
        String t = req.getTarget().getNumber().toLowerCase();
        double a = req.getAmount();

        Account source = accountsRepo.findByNumber(s);

        if(a<source.getBalance()){
            System.out.println("ACCOUNT  -| TRANSFER:: ");
            withdraw(s, a);
            deposit( t, a);

            return new TransferResponse(mapper.accountToDTO(accountsRepo.findByNumber(s)), mapper.accountToDTO(accountsRepo.findByNumber(t)), a);
        }
        return null;
    }              //
}
