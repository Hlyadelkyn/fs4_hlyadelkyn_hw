package app.service;

import app.DTO.request.CustomerRequest;
import app.DTO.response.DetailedCustomerResponse;
import app.DTO.response.PublicCustomerResponse;
import app.exceptions.CustomerNotFoundEx;
import app.mappers.Facade;
import app.model.Customer;
import app.repo.CustomersRepo;
import app.security.CustomPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;


@Service
public class CustomersService {
    @Autowired
    private CustomersRepo customerRepo;

    @Autowired
    CustomPasswordEncoder encoder;

    @Autowired
    private Facade mapper;

    private static final int pageSize = 10;

    CustomersService(CustomersRepo cr) {
        customerRepo = cr;
    }

    public List<PublicCustomerResponse> getAt(int page) {
        Pageable pageable = PageRequest.of(page, pageSize);
        Page<Customer> customerPage = customerRepo.findAll(pageable);

        return customerPage.map(c -> mapper.customerToPublicDTO(c)).toList();
    }

    public Customer                     getById(long id) {
        return customerRepo.findById(id).orElseThrow(() -> new CustomerNotFoundEx(id));
    }
    public DetailedCustomerResponse     getDTOById(long id){
        return customerRepo.findById(id).map(customer -> mapper.customerToDetailedDTO(customer)).orElseThrow(() -> new CustomerNotFoundEx(id));
    }

    public DetailedCustomerResponse     create(CustomerRequest req) {
        Customer parsed = mapper.customerFromDTO(req);
        String ps = parsed.getPassword();
        parsed.setPassword(encoder.getPasswordEncoder().encode(ps));
        Customer saved = customerRepo.save(parsed);
        return   mapper.customerToDetailedDTO(saved);
    }

    public boolean                      delete(long id) {
        try {
            customerRepo.deleteById(id);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public DetailedCustomerResponse     edit(long id, CustomerRequest req) {
        try {
            Customer c = customerRepo.findById(id).get();

            if(Objects.equals(c.getPassword(), req.getPassword())){
                if (req.getName() != null) {
                    c.setName(req.getName());
                }
                if (req.getEmail() != null) {
                    c.setEmail(req.getEmail());
                }
                if (req.getAge() != null) {
                    c.setAge(req.getAge());
                }
                if (req.getPhoneNumber() != null) {
                    c.setPhoneNumber(req.getPhoneNumber());
                }
                customerRepo.save(c);
                return mapper.customerToDetailedDTO(c);
            }else{
                return null;
            }
        }catch (NoSuchElementException ex){
            throw new CustomerNotFoundEx(id);
        }
    }
}
