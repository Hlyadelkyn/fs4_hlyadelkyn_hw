package app.repo;

import app.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountsRepo extends JpaRepository<Account, Long> {
    Account findByNumber(String number);
}
