package app.repo;

import app.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomersRepo extends JpaRepository<Customer, Long> {

    Customer findByName(String name);
    Optional<Customer> getByName(String name);

}
