package app.repo;

import app.model.Employer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployersRepo extends JpaRepository<Employer, Long> {
}
