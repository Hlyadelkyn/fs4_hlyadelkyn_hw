package app.mappers;

import app.DTO.request.CustomerRequest;
import app.DTO.response.DetailedCustomerResponse;
import app.DTO.response.PublicCustomerResponse;
import app.model.Customer;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class CustomerMapper {
    @Autowired
    private ModelMapper mapper;

    public Customer toEntity(CustomerRequest req) {
        return Objects.isNull(req) ? null : mapper.map(req, Customer.class);
    }
    public PublicCustomerResponse toPublicDto(Customer ent){
        return Objects.isNull(ent) ? null : mapper.map(ent , PublicCustomerResponse.class);
    }
    public DetailedCustomerResponse toDetailedDto(Customer ent){
        return Objects.isNull(ent) ? null : mapper.map(ent , DetailedCustomerResponse.class);
    }

}
