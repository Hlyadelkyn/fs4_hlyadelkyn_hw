package app.mappers;

import app.DTO.request.EmployerRequest;
import app.DTO.response.DetailedEmployerResponse;
import app.DTO.response.PublicEmployerResponse;
import app.model.Employer;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class EmployerMapper {
    @Autowired
    ModelMapper mapper;
    public Employer toEntity(EmployerRequest req){
        return Objects.isNull(req)? null: mapper.map(req,Employer.class);
    }
    public DetailedEmployerResponse toDTO(Employer ent){
        return Objects.isNull(ent)? null: mapper.map(ent, DetailedEmployerResponse.class);
    }
    public PublicEmployerResponse toPublicDTO(Employer ent){
        return Objects.isNull(ent)? null: mapper.map(ent, PublicEmployerResponse.class);
    }
}
