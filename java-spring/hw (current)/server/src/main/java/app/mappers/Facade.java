package app.mappers;

import app.DTO.request.CustomerRequest;
import app.DTO.response.AccountResponse;
import app.DTO.response.DetailedCustomerResponse;
import app.DTO.response.DetailedEmployerResponse;
import app.DTO.response.PublicCustomerResponse;
import app.model.Account;
import app.model.Customer;
import app.model.Employer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


//import app.DTO.request.AccountRequest;
//import app.DTO.request.EmployerRequest;

import java.util.Objects;

@Service
public class Facade {
    @Autowired
    CustomerMapper customerMapper;

    @Autowired
    AccountMapper accountMapper;

    @Autowired
    EmployerMapper employerMapper;

    public Customer customerFromDTO(CustomerRequest req) {
        return customerMapper.toEntity(req);
    }

    public PublicCustomerResponse customerToPublicDTO(Customer ent) {
        return customerMapper.toPublicDto(ent);
    }

    public DetailedCustomerResponse customerToDetailedDTO(Customer ent) {
        return customerMapper.toDetailedDto(ent);
    }


    public AccountResponse accountToDTO(Account ent) {
        return accountMapper.toDto(ent);
    }


    public DetailedEmployerResponse employerToDTO(Employer ent) {
        return Objects.isNull(ent) ? null : employerMapper.toDTO(ent);
    }
}


//    public Employer employerFromDTO(EmployerRequest req) {
//        return Objects.isNull(req) ? null : employerMapper.toEntity(req);
//    }
//    public Account accountFromDTO(AccountRequest req) {
//        return accountMapper.toEntity(req);
//    }