package app.mappers;

import app.DTO.request.AccountRequest;
import app.DTO.response.AccountResponse;
import app.model.Account;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class AccountMapper {
    @Autowired
    private ModelMapper mapper;

    public Account toEntity(AccountRequest req){
        return  Objects.isNull(req) ? null : mapper.map(req, Account.class);
    }
    public AccountResponse toDto(Account ent){
        return Objects.isNull(ent)? null: mapper.map(ent, AccountResponse.class);
    }
}
