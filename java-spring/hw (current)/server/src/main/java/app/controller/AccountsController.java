package app.controller;

import app.DTO.response.AccountResponse;
import app.DTO.request.TransferRequest;
import app.DTO.response.TransferResponse;
import app.service.AccountsService;

import app.service.CustomersService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("accounts")
@CrossOrigin(origins = "http://localhost:8080")
public class AccountsController {
    @Autowired CustomersService customerServ;
    @Autowired AccountsService accountServ;

    @PutMapping("/deposit")  public AccountResponse  deposit( @RequestBody TransferRequest req){
        log.info("DEPOSIT REQUEST: " + req.toString());
        return accountServ.deposit(req);
    }
    @PutMapping("/withdraw") public AccountResponse  withdraw(@RequestBody TransferRequest req){
        log.info("WITHDRAW REQUEST: " + req.toString());
        return accountServ.withdraw(req);
    }
    @PutMapping("/transfer") public TransferResponse transfer(@RequestBody TransferRequest req){
        log.info("TRANSFER REQUEST: " + req.toString());
        return accountServ.transfer(req);
    }

}