package app.controller;

import app.DTO.request.AccountRequest;
import app.DTO.request.CustomerRequest;
import app.DTO.response.AccountResponse;
import app.DTO.response.DetailedCustomerResponse;
import app.DTO.response.PublicCustomerResponse;
import app.service.AccountsService;
import app.service.CustomersService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:8080", methods = {RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.GET, RequestMethod.POST})
@Slf4j @RestController @RequiredArgsConstructor @RequestMapping("users") public class CustomersController {
    @Autowired CustomersService customerServ;
    @Autowired AccountsService accountServ;

    //Customer management______________________________
    @GetMapping(   "")             public     List<PublicCustomerResponse> getAtPage     (@RequestParam(defaultValue = "1") int page)             {
        log.info("GET    PAGE<CUSTOMERS> AT "+ page +" PAGE REQUEST");
        return customerServ.getAt(page - 1);
    }
    @GetMapping(   "/{id}")        public         DetailedCustomerResponse getById       (@PathVariable long     id)                              {
        log.info("GET    CUSTOMER WITH ID : " + id +" REQUEST");
        return customerServ.getDTOById(id);
    }
    @PostMapping(  "/create")      public         DetailedCustomerResponse createCustomer(@Valid @RequestBody    CustomerRequest req)             {
        log.info("CREATE CUSTOMER : " + req.toString()+" REQUEST");
        return customerServ.create(req);
    }
    @PutMapping(   "/{id}/edit")   public         DetailedCustomerResponse editCustomer  (@PathVariable long id, @RequestBody CustomerRequest req){
        log.info("EDIT   CUSTOMER WITH ID : " + id +" REQUEST : " + req.toString());
        return customerServ.edit(id, req);
    }
    @DeleteMapping("/delete/{id}") public         Boolean                  deleteCustomer(@PathVariable long id)                                  {
        log.info("DELETE CUSTOMER WITH ID : "+ id + " REQUEST ");
        return customerServ.delete(id);
    }
    //Account management______________________________
    @PostMapping(  "/{id}/create-account") public AccountResponse          createAccount (@PathVariable long id, @RequestParam String currency)   {
        log.info("CREATE ACCOUNT(" + currency+") FOR CUSTOMER WITH ID: " + id +" REQUEST");
        return accountServ.createAccount(id, currency);
    }
    @DeleteMapping("/{id}/delete-account") public Boolean                  deleteAccount (@PathVariable long id, @RequestBody AccountRequest req) {
        log.info("DELETE ACCOUNT : "+ req.toString() + " ON USER WITH ID : " +id + " REQUEST ");
        return accountServ.deleteAccount(id, req);
    }
}
