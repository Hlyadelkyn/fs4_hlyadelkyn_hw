package app.controller;

import app.DTO.request.EmployerRequest;
import app.DTO.response.DetailedEmployerResponse;
import app.service.CustomersService;
import app.service.EmployersService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8080", methods = {RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.GET, RequestMethod.POST})
@Slf4j @RestController @RequiredArgsConstructor @RequestMapping("emp") public class EmployersController{
    @Autowired CustomersService customerServ;
    @Autowired EmployersService empServ;

    @GetMapping( "")           public List<DetailedEmployerResponse>  getAll        (){
        log.info("GETALL  EMPLOYER REQUEST");
        return empServ.getAll();
    }
    @GetMapping( "/{id}")            public DetailedEmployerResponse  getOne        (@PathVariable long id){
        log.info("GETONE  EMPLOYER <" + id + ">");
        return empServ.findById(id);
    }
    @PostMapping("/create")          public DetailedEmployerResponse  createOne     (@RequestBody EmployerRequest req)  {
        log.info("CREATE  EMPLOYER REQUEST : " + req);
        return empServ.create(req);
    }
    @PostMapping("/link-customer")   public DetailedEmployerResponse  linkCustomer  (@RequestParam long cid, @RequestParam long eid){
        log.info("LINK   EMPLOYER-CUSTOMER IDs:<" + eid +" " + cid + "> REQUEST");
        return empServ.link(eid, cid);
    }
    @PostMapping("/unlink-customer") public DetailedEmployerResponse  unlinkCustomer(@RequestParam long cid, @RequestParam long eid){
        log.info("UNLINK EMPLOYER-CUSTOMER IDs:<" + eid +" " + cid + "> REQUEST");
        return empServ.unlink(eid, cid);
    }
}
