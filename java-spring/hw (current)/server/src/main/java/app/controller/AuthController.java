package app.controller;

import app.DTO.request.LoginRequest;
import app.DTO.response.LoginResponse;
import app.model.Customer;
import app.repo.CustomersRepo;
import app.security.CustomPasswordEncoder;
import app.security.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j @RestController @RequestMapping("/auth/") @RequiredArgsConstructor public class AuthController {

    private final CustomersRepo         crepo;
    private final CustomPasswordEncoder enc;
    private final JwtService            jserv;


    @PostMapping("/login") public LoginResponse handleLogin(@RequestBody LoginRequest req){
        log.info("LOGIN REQUEST : " + req.toString());

        Customer c = crepo.findByName(req.getUsername());
        if(c!=null){
            log.info("CUSTOMER FOUND : " + c);
            if(enc.getPasswordEncoder().matches(req.getPassword(), c.getPassword())){ // було if(enc.getPasswordEncoder().matches(req.getUsername(), c.getPassword())){
                                                                                      // тому працював тільки admin admin і то яким чином звичайно загадка бо енкодер
                                                                                      // перевіряє значення з хешом
                LoginResponse resp = new LoginResponse(true, "",jserv.generate(c.getId()));
                return resp;
            }
        }
        return new LoginResponse(false, "bad credentials", null);
    }
}