package app.service;

import app.DTO.request.AccountRequest;
import app.DTO.request.TransferRequest;
import app.DTO.response.AccountResponse;
import app.DTO.response.TransferResponse;
import app.mappers.Facade;
import app.model.Account;
import app.model.Currency;
import app.model.Customer;
import app.repo.AccountsRepo;
import app.repo.CustomersRepo;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class) @SpringBootTest public class AccountServiceTest {
    @MockBean  private CustomersRepo     customersRepo;
    @MockBean  private AccountsRepo      accountsRepo;
    @Autowired private AccountsService   accountsService;

    @Test public void AccountService_GenericTest(){
        Integer   s_id   = 1;  final String  s_number = UUID.randomUUID().toString(); final Currency s_curr = Currency.UAH;
        Integer   t_id   = 2;  final String  t_number = UUID.randomUUID().toString(); final Currency t_curr = Currency.UAH;
        final double amount = 200D; final double s_balance = 500D; final double t_balance = 0D;

        Customer s_subject = new Customer();  s_subject.setId(Long.parseLong(s_id.toString())); s_subject.setAccounts(new ArrayList<>());
        Customer t_subject = new Customer();  t_subject.setId(Long.parseLong(t_id.toString())); t_subject.setAccounts(new ArrayList<>());

        Account source  = new Account();  source.setNumber(s_number); source.setCustomer(s_subject);
        Account target  = new Account();  target.setNumber(t_number); target.setCustomer(t_subject);
        source.setBalance(s_balance);     source.setCurrency(s_curr); source.setId(Long.parseLong(s_id.toString()));
        target.setBalance(t_balance);     target.setCurrency(t_curr); target.setId(Long.parseLong(t_id.toString()));
        // creating requests & pre-expected responses
        AccountRequest   source_req    =  new AccountRequest(s_id, s_number);
        AccountRequest   target_req    =  new AccountRequest(t_id, t_number);
        TransferRequest  transfer_req  =  new TransferRequest(source_req,   target_req,  amount);

        AccountResponse  source_resp   =  new AccountResponse(s_id, s_number, s_balance, s_curr);
        AccountResponse  target_resp   =  new AccountResponse(t_id, t_number, t_balance, t_curr);
        TransferResponse transfer_resp =  new TransferResponse(source_resp, target_resp, amount);

        target_resp.setBalance(amount);
        source_resp.setBalance(s_balance - amount);
        // mocking repo
        when(customersRepo.save(s_subject)).thenReturn(s_subject);    when(customersRepo.save(t_subject)).thenReturn(t_subject);

        when(accountsRepo.findByNumber(t_number)).thenReturn(target); when(customersRepo.findById(Long.parseLong(t_id.toString()))).thenReturn(Optional.of(t_subject));
        when(accountsRepo.findByNumber(s_number)).thenReturn(source); when(customersRepo.findById(Long.parseLong(s_id.toString()))).thenReturn(Optional.of(s_subject));

        // TODO: write all tests for Accounts Service till 12PM 14/08;
        Assertions.assertThat(accountsService.createAccount(1L, "usd")).isNotNull().isNotSameAs(source_resp).isNotSameAs(target_resp);

        Assertions.assertThat(accountsService.deposit(transfer_req)) .isNotNull().isEqualTo(target_resp);  // depo
        Assertions.assertThat(accountsService.withdraw(transfer_req)).isNotNull().isEqualTo(source_resp);  // withdraw
        target_resp.setBalance(amount * 2); source_resp.setBalance(s_balance - (amount*2)); //changing expected values
        Assertions.assertThat(accountsService.transfer(transfer_req)).isNotNull().isEqualTo(transfer_resp);// transfer

    }
}
