package app.service;

import app.DTO.request.CustomerRequest;
import app.DTO.response.DetailedCustomerResponse;
import app.DTO.response.PublicCustomerResponse;
import app.mappers.Facade;
import app.model.Customer;
import app.repo.AccountsRepo;
import app.repo.CustomersRepo;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class) @SpringBootTest
public class CustomerServiceTest {
    @MockBean  private CustomersRepo       customersRepo;
    @Autowired private CustomersService customersService;

    @Test
    public void CustomerService_GenericTest(){
        //subtraction repetitive and sensible content
        final long c_id = 1L;
        final String c_name = "test";
        final String c_email = "test@example.com";
        final String c_phoneNumber = "+38 093 111 1111";
        final String c_password = "teST!@#1ead";
        final int    c_age = 25;
        //creating model
        Customer mockedCustomer = new Customer(c_name, c_email , c_age);
        mockedCustomer.setId(c_id);
        mockedCustomer.setPhoneNumber(c_phoneNumber);
        mockedCustomer.setPassword(c_password);
        //creating pageable
        List<Customer> c_list = List.of(mockedCustomer);
        Page<Customer> c_page = new PageImpl<>(c_list);
        //creating requests & expected responses
        CustomerRequest                 req = new CustomerRequest(c_name, c_email, c_age, c_password, c_phoneNumber);
        PublicCustomerResponse   p_pre_resp = new PublicCustomerResponse(Integer.parseInt(Long.toString(c_id)), c_name, List.of());
        DetailedCustomerResponse d_pre_resp = new DetailedCustomerResponse(Integer.parseInt(Long.toString(c_id)), c_name, c_email,c_age, List.of(),
                null, c_phoneNumber);
        //mocking repository
        when(customersRepo.save(Mockito.any(Customer.class))).thenReturn(mockedCustomer);
        when(customersRepo.findAll(Mockito.any(Pageable.class))).thenReturn(c_page);
        when(customersRepo.findById(Mockito.any(Long.class))).thenReturn(Optional.of(mockedCustomer));

        //                    obtaining responses              | not-null & equality assertion
        Assertions.assertThat(customersService.create(req))    .isNotNull().isEqualTo(d_pre_resp);//                 create
        Assertions.assertThat(customersService.getAt(1)) .isNotNull().isEqualTo(List.of(p_pre_resp));//        get at
        Assertions.assertThat(customersService.getDTOById(1L)) .isNotNull().isEqualTo(d_pre_resp);//                 get Dto by id
        Assertions.assertThat(customersService.edit(c_id, req)).isNotNull().isEqualTo(d_pre_resp);//                 edit
        Assertions.assertThat(customersService.delete(c_id))   .isEqualTo(true);//                                   delete
    }
}
