package app.controller;


import app.DTO.request.CustomerRequest;
import app.DTO.response.DetailedCustomerResponse;
import app.mappers.Facade;
import app.service.CustomersService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(controllers = CustomersController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class) public class CustomerControllerTest {
    @InjectMocks CustomersController customersController;
    @InjectMocks  CustomersService   customersService;

    @Test public void CustomerControllerTest() throws Exception {
//        String c_name = "test"; String c_email = "test@example.com"; String c_password = "tEsT!@#22124"; String c_phoneNumber ="+38 093 111 1111"; int c_age = 25;
//        CustomerRequest          c_req = new CustomerRequest(c_name, c_email, c_age, c_password, c_phoneNumber);
//        DetailedCustomerResponse d_c_resp = new DetailedCustomerResponse(1, c_name, c_email, c_age, new ArrayList<>(), new ArrayList<>(), c_phoneNumber );
//
//        when(customersService.create(Mockito.any(CustomerRequest.class))).thenReturn(d_c_resp);
//        Assertions.assertThat(customersController.createCustomer(c_req)).isNotNull().isEqualTo(d_c_resp);
    }
}
// як не намагався вирішити, не зміг.
// Cannot invoke "app.service.CustomersService.create(app.DTO.request.CustomerRequest)" because "this.customersService" is null
