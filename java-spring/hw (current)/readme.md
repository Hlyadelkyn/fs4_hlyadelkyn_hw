запуск сервера        : server/src/main/java/app/App.main
запуск клієнт сторони : client/ npm i -> npm start

найбільш часті запроси на сервер

GET    all users          http://localhost:9000/users

POST   create customer    http://localhost:9000/users/create?name=&email=&age=

DEL    delete customer    http://localhost:9000/users/delete/id

POST   create account     http://localhost:9000/users/id/create-account?currency=

DEL    delete account     http://localhost:9000/users/id/delete-account?accountId=

PUT    edit customer      http://localhost:9000/users/id/edit?name=&email=&age=

PUT    deposit            http://localhost:9000/accounts/deposit?amount=&target=

PUT    withdraw           http://localhost:9000/accounts/withdraw?amount=&target=

PUT    transfer           http://localhost:9000/accounts/transfer?source=&target=&amount=