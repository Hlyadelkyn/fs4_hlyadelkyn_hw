package ht12;
import java.io.IOException;
import java.util.List;
import java.util.Set;

class FamilyOverflowException extends RuntimeException {
	public FamilyOverflowException(String errorMessage) {
		super(errorMessage);
	}
}

public class FamilyController {
	FamilyService familyService;

	FamilyController(FamilyService a) {
		this.familyService = a;
	}

	List<Family> getAllFamilies() {
		return familyService.getAllFamilies();
	}

	void displayALlFamilies() {
		familyService.displayAllFamilies();
	}
	void displayAllFamiliesPretty(){
		familyService.displayAllFamiliesPretty();
	}
	void displayInputedPretty(List<Family>inputL){
		familyService.displayInputedPretty(inputL);
	}

	List<Family> getFamiliesBiggerThan(int inputindex) {
		return familyService.getFamiliesBiggerThan(inputindex);
	}

	List<Family> getFamiliesLessThan(int inputIndex) {
		return familyService.getFamiliesLessThan(inputIndex);
	}

	int countFamiliesWithMemberNumber(int inputNum) {
		return familyService.countFamiliesWithMemberNumber(inputNum);
	}

	void createNewFamily(Human h0, Human h1) {
		familyService.createNewFamily(h0, h1);
	}

	void deleteFamilyByIndex(int inputIndex) {
		familyService.deleteFamilyByIndex(inputIndex);
	}

	void bornChild(Family f0, String name, String nameW) {
		if(f0.getChildren().size() <=5){
			familyService.bornChild(f0, name, nameW);
		}else{
			throw new FamilyOverflowException("family is to big");
		}
	}

	void adoptChild(Family f0, Human ch0) {
		if(f0.getChildren().size() <=5){
			familyService.adoptChild(f0, ch0);
		}else{
			throw new FamilyOverflowException("family is to big");
		}
	}

	void deleteAllChildrenOlderThen(int inputIndex) {
		familyService.deleteAllChildrenOlderThen(inputIndex);
	}

	int count() {
		return familyService.count();
	}

	Family getFamilyById(int id) {
		return familyService.getFamilyById(id);
	}

	Set<Pet> getPets(int id) {
		return familyService.getPets(id);
	}

	void addPet(int id, Pet p0) {
		familyService.addPet(id, p0);
	}

	void save() throws IOException {
		familyService.saveToLocal();
		System.out.println("---SAVED " + familyService.getAllFamilies().size() + " FAMILIES---");
	}

	void load() throws IOException, ClassNotFoundException {
		familyService.loadFromLocal();
		System.out.println("---LOADED " + familyService.getAllFamilies().size() + " FAMILIES---");
	}
}
