package ht12;

import java.io.Serializable;
import java.util.*;

interface SpeciecActions  {
	void foul();
}

public abstract class Pet implements Serializable {
	private Species species;
	private String nickname;
	private int age;
	private int trickLevel;
	private Set<String> habits;
	Pet() {}

	Pet(String inputNickname) {

		nickname = inputNickname;
	}

	Pet(String inputNickname, int inputAge, int inputTrickLevel, Set<String>inputHabits) {
		if (inputTrickLevel > 100 || inputTrickLevel < 0) throw new IllegalArgumentException("Trick level should be in the range of [0-100].");

		nickname = inputNickname;
		age = inputAge;
		trickLevel = inputTrickLevel;
		habits = inputHabits;
	}

	void main(String[] args) {
		this.setSpecies(Species.UNKNOWN);
	}


	void eat() {System.out.println("I`m eating!");}

	abstract void respond();

	@Override
	public String toString() {
		if(habits == null ){
			return String.format("%s{nickname='%s', age=%s, trick level=%s", species.name(), nickname, age, trickLevel);
		}
		return String.format("%s{nickname='%s', age=%s, trick level=%s, habits=%s", species.name(), nickname, age, trickLevel, habits.toString());
	}

	@Override
	public boolean equals(Object inputObj) {
		if (!(inputObj instanceof Pet)) return false;
		if (inputObj instanceof Pet && ((Pet) inputObj).getAge() == this.age && ((Pet) inputObj).getNickname() == this.nickname)
			return true;
		return false;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public int getTrickLevel() {
		return trickLevel;
	}

	public void setTrickLevel(int trickLevel) {if (trickLevel < 100 && trickLevel > 0) this.trickLevel = trickLevel;}


	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Species getSpecies() {
		return species;
	}

	public void setSpecies(Species species) {
		this.species = species;
	}

	public Set<String> getHabits() {
		return habits;
	}

	public void setHabits(Set<String> habits) {
		this.habits = habits;
	}

	protected void finalize() throws Throwable {
		try {
			System.out.println("Pet" + this.toString() + " deleted");
		} catch (Throwable e) {
			throw e;
		} finally {
//			System.out.println("inside Human class finalize()");
			super.finalize();
		}
	}
};

class Fish extends Pet {
	Fish() {
		super();
		super.setSpecies(Species.Fish);
	}

	Fish(String inputNickname) {
		super(inputNickname);
		super.setSpecies(Species.Fish);
	}

	Fish(String inputNickname, int inputAge, int inputTrickLevel, Set<String> inputHabits) {
		super(inputNickname, inputAge, inputTrickLevel, inputHabits);
		super.setSpecies(Species.Fish);
	}

	void respond() {
		System.out.println(String.format("Hi master! It`s me, fish %s, I missed you", super.getNickname()));
	}
}

class DomesticCat extends Pet implements SpeciecActions {
	DomesticCat() {
		super();
		super.setSpecies(Species.Cat);
	}


	DomesticCat(String inputNickname) {
		super(inputNickname);
		super.setSpecies(Species.Cat);
	}

	DomesticCat(String inputNickname, int inputAge, int inputTrickLevel,Set<String> inputHabits) {
		super(inputNickname, inputAge, inputTrickLevel, inputHabits);
		super.setSpecies(Species.Cat);
	}

	void respond() {
		System.out.println(String.format("Hi master! It`s me,domestic cat %s, I missed you", super.getNickname()));
	}

	public void foul() {
		System.out.println("Domestic cat fouled");
	}
}

class Dog extends Pet implements SpeciecActions {
	Dog() {
		super();
		super.setSpecies(Species.Dog);
	}

	Dog(String inputNickname) {
		super(inputNickname);
		super.setSpecies(Species.Dog);
	}

	Dog(String inputNickname, int inputAge, int inputTrickLevel, Set<String>  inputHabits) {
		super(inputNickname, inputAge, inputTrickLevel, inputHabits);
		super.setSpecies(Species.Dog);
	}

	void respond() {
		System.out.println(String.format("Hi master! It`s me, dog %s, I missed you", super.getNickname()));
	}

	public void foul() {
		System.out.println("Dog fouled");
	}
}


class RoboCat extends Pet {
	RoboCat() {
		super();
		super.setSpecies(Species.Robocat);
	}

	RoboCat(String inputNickname) {
		super(inputNickname);
		super.setSpecies(Species.Robocat);
	}

	RoboCat(String inputNickname, int inputAge, int inputTrickLevel,Set<String> inputHabits) {
		super(inputNickname, inputAge, inputTrickLevel, inputHabits);
		super.setSpecies(Species.Robocat);
	}

	void respond() {
		System.out.println(String.format("Hi master! It`s me, rObO-cAt %s, I missed you", super.getNickname()));
	}

}
