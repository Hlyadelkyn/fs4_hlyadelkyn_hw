package ht12;

import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.io.*;

public class FamilyService {
	CollectionFamilyDao FamilyDao;

	FamilyService(CollectionFamilyDao a){
		this.FamilyDao = a;
	}

	List<Family> getAllFamilies() {
		return FamilyDao.getAllFamilies();
	}

	void displayAllFamilies() {
		List<Family> FamilyList = FamilyDao.getAllFamilies();
		int[] idx = {0};
		FamilyList.forEach((family -> {                   //Since: 1.8
			System.out.println(idx[0]++ + " " + family);  //Since: 1.8
		}));
	}
	void displayInputedPretty(List<Family> inputList) {
		if(inputList.size() ==0){
			System.out.println("-| NO FAMILIES FOUND");
		}else{
			System.out.println("-| IN ALL: "+ inputList.size() + " FAMILIES");
			int[] idx = {1	};
			inputList.forEach((family -> {                   //Since: 1.8
				System.out.println(idx[0]++ + " " + family.prettyFormat());  //Since: 1.8
			}));
		}

	}
	void displayAllFamiliesPretty(){
		List<Family> FamilyList = FamilyDao.getAllFamilies();
		System.out.println("-| IN ALL: "+ FamilyDao.getAllFamilies().size() + " FAMILIES");
		int[] idx = {1};
		FamilyList.forEach((family -> {                   //Since: 1.8
			System.out.println(idx[0]++ + " " + family.prettyFormat());  //Since: 1.8
		}));
	}

	List<Family> getFamiliesBiggerThan(int inputAmount) {
		Predicate<Family> byCount = family -> family.countFamily() > inputAmount;  //Since: 1.8
		List<Family> req = FamilyDao.getAllFamilies();
		Stream<Family> res = req.stream().filter(byCount);                         //Since: 1.8
		return res.collect(Collectors.toList());                                   //Since: 1.8
	}

	List<Family> getFamiliesLessThan(int inputAmount) {
		Predicate<Family> byCount = family -> family.countFamily() < inputAmount;  //Since: 1.8
		List<Family> req = FamilyDao.getAllFamilies();
		Stream<Family> str0 = req.stream().filter(byCount);                        //Since: 1.8
		return str0.collect(Collectors.toList());                                  //Since: 1.8
	}
	int countFamiliesWithMemberNumber (int inputAmount){
		Predicate<Family> byCount = family -> family.countFamily() == inputAmount; //Since: 1.8
		List<Family> req = FamilyDao.getAllFamilies();
		Stream<Family> str0 = req.stream().filter(byCount);                        //Since: 1.8
		return str0.collect(Collectors.toList()).size();                           //Since: 1.8
	}
	void createNewFamily(Human h0, Human h1){
		Family f0 = new Family(h0, h1);
		FamilyDao.saveFamily(f0);
	}
	void deleteFamilyByIndex(int inputIndex){
		FamilyDao.deleteFamily(inputIndex);
	}
	void bornChild(Family f0, String nameM, String nameW){
		int temp = (Math.random() <= 0.5) ? 1 : 2;
		Human ch0;
		if (temp ==1){
			ch0 = new Man(nameM, f0.getFather().getSurname(), System.currentTimeMillis());
		}else{
			ch0 = new Woman(nameW, f0.getFather().getSurname(), System.currentTimeMillis());
		}
		f0.addChild(ch0);
		FamilyDao.saveFamily(f0);
	}
	void adoptChild(Family f0, Human ch0){
		ch0.setFamily(f0);
		f0.addChild(ch0);
		FamilyDao.saveFamily(f0);
	}
	void deleteAllChildrenOlderThen (int inputAmount){
		Predicate<Human> byAge = human -> human.getAge() < inputAmount; //Since: 1.8
		List<Family> l0 = FamilyDao.getAllFamilies();
		l0.forEach(family -> {                                          //Since: 1.8
			List<Human> chl0 = family.getChildren();
			Stream<Human> str = chl0.stream().filter(byAge);            //Since: 1.8
			List<Human> chl1 = str.toList();
			ArrayList<Human> chl2 = new ArrayList<>(chl1);
			family.setChildren(chl2);
			FamilyDao.saveFamily(family);
		});
	};
	int count(){
		return FamilyDao.getAllFamilies().size();
	}
	Family getFamilyById (int inputIndex){
		return FamilyDao.getByFamilyIndex(inputIndex);
	}
	Set<Pet> getPets(int inputIndex){
		return FamilyDao.getByFamilyIndex(inputIndex).getPet();
	}
	void addPet (int inputIndex, Pet inputPet) {
		Family f0 = FamilyDao.getByFamilyIndex(inputIndex);

		Set<Pet> s0 = f0.getPet();
		if (s0 == null) {
			Set<Pet> s1 = new HashSet<>();
			s1.add(inputPet);
			f0.setPet(s1);
		} else {
			s0.add(inputPet);
			f0.setPet(s0);
		}
		FamilyDao.saveFamily(f0);
	}

	void loadData(List<Family> inputLD) {
		if (inputLD.size() > 0) {
			FamilyDao.loadData(inputLD);
		}
	}

	void saveToLocal() throws IOException {
		List<Family> families = FamilyDao.getAllFamilies();
		FileOutputStream fos = new FileOutputStream("save.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(families);
		oos.close();
	}

	void loadFromLocal() throws IOException, ClassNotFoundException {
		FileInputStream fis = new FileInputStream("save.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		List<Family> families = (List<Family>) ois.readObject();
		ois.close();
		FamilyDao.loadData(families);
	}

}
