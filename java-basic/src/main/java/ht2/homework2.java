package ht2;

import java.util.Random;
import java.io.InputStream;
import java.util.Scanner;

public class homework2 {

     public static String representCell(int value) {
           if(value==3) return "x";
          if (value == 2) return "*";
          return "-";
     }
     public static void printField(int[][]field){

          for (int y = 0; y < field.length; y++) {
               System.out.print("|");
               for (int x = 0; x < field[y].length; x++) {
                    System.out.printf(" %s |", representCell(field[x][y]));
               }
               System.out.println();

          }
     }

     public static int[][] initField() {
          int[][] field = new int[5][5]; // init the empty field

          Random random = new Random();
          int fieldRandX = random.nextInt(0,5);
          int fieldRandY = random.nextInt(0,5);

          field[fieldRandX][fieldRandY] = 1; //setting the target
          return field;
     }
     public static void main(String[] args) {
          InputStream in = System.in;
          var scanner = new Scanner(in);

          int[][]field = initField();// init the field
          System.out.println("All set. Get ready to rumble!");//greeting


          while (true){
               printField(field);
               try {

                    System.out.print("Enter x coord -> ");
                    String userNumX = scanner.nextLine();
                    int userCordX =  Integer.parseInt(userNumX) -1;
                    System.out.print("Enter y coord -> ");
                    String userNumY = scanner.nextLine();
                    int userCordY = field.length - Integer.parseInt(userNumY);

                    if (userCordX > field.length || userCordX<0 || userCordY > field.length || userCordY < 0) {
                         System.out.print("incorrect coordinates");
                    } else if(field[userCordX][userCordY] == 1) {
                         System.out.println(String.format("Congrats you have won!"));
                         field[userCordX][userCordY] = 3; // informing to render the target
                         printField(field);
                         break;
                    }else{
                         field[userCordX][userCordY] =2;
                    }
               } catch (NumberFormatException ex) {
                    System.out.print("That s not a number or at least integer one");
               }
          }
     }

}
