package ht11;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;

public class FamilyManager {

	CollectionFamilyDao CFD = new CollectionFamilyDao();
	FamilyService FS = new FamilyService(CFD);
	FamilyController FC = new FamilyController(FS);

	Scanner sc = new Scanner(System.in);
	String greetings = """
			   
			---------HI CHAMPION, THIS IS FAMILYMANAGER v0.1---------
			""";
	String menu = """
			   
			|  SELECT AN ACTION:
				1  -  Fill up Db with TESTDATA                                          (no input required)
				2  -  Print all families                                                (no input required)
				3  -  Print all families where the number of people is greater than     (input required)
				4  -  Print all families where the number of people is less than        (input required)
				5  -  Count all families where the number of people equals to           (input required)
				6  -  Create new family                                                 (multiple inputs required)
				7  -  Delete family by ID                                               (input required)
				8  -  Manipulate with specific family by ID                             (multiple inputs required)
				9  -  Delete all children older than                                    (input required)
				10 -  Save to local file                                                (no input required)
				11 -  Load from local file                                              (no input required)
						""";
	String ERRORMSG = "------------ERROR OCCURRED, PLEASE TRY AGAIN------------ \n";
	boolean testDataFilled = false;

	void run() {
		System.out.println(greetings);
		while (true) {
			System.out.println(menu);
			System.out.print(">>> ");
			String input = sc.next();
			if (Objects.equals(input, "exit")) {
				System.out.println("------------------------ALL BEST!-----------------------");
				break;

			} else {
				try {
					int inputInt = Integer.parseInt(input);
					clear();
					handleInput(inputInt);
				} catch (Exception ex) {

					System.out.println(ERRORMSG + ex);
				}
			}
		}
	}

	void fillWithTestData() {
		System.out.println("------| TESTDATA UPLOADING...");
		Man m0 = new Man("Walter", "White", "7/09/1958", 124);
		Man m1 = new Man("Tyler", "Durden", "01/11/1989", 102);
		Man m2 = new Man("Rogal", "Dorn", "01/09/1970", 120);
		Man m3 = new Man("Patrick", "Bateman", "23/10/1962", 110);

		Woman w0 = new Woman("Leia", "Organa", "01/01/1970", 99);
		Woman w1 = new Woman("Helot", "Secundus", "01/09/1970", 115);
		Woman w2 = new Woman("Lara", "Croft", "14/2/1968", 101);
		Woman w3 = new Woman("Sasha", "Kladbische", "01/01/1999", 105);

		int preNum = FC.getAllFamilies().size();

		FC.createNewFamily(w0, m0);
		FC.createNewFamily(w1, m1);
		FC.createNewFamily(w2, m2);
		FC.createNewFamily(w3, m3);
		System.out.println("------| " + FC.getAllFamilies().size() + " FAMILIES CREATED");


		Family f0 = FC.getFamilyById(preNum);
		Family f1 = FC.getFamilyById(preNum + 1);
		Family f2 = FC.getFamilyById(preNum + 2);
		Family f3 = FC.getFamilyById(preNum + 3);

		Fish p0 = new Fish("Neo");
		DomesticCat p1 = new DomesticCat("Garfield");
		Dog p2 = new Dog("Patron");
		DomesticCat p3 = new DomesticCat("Puss in the Boots ");
		RoboCat p4 = new RoboCat("Servitor");

		FC.addPet(0, p0);
		FC.addPet(0, p1);
		FC.addPet(1, p2);
		FC.addPet(2, p3);
		FC.addPet(3, p4);

		FC.bornChild(f0, "Wader", "Ahsoka");
		FC.bornChild(f1, "Edward", "Marla");
		FC.bornChild(f2, "Roboute", "Leith");
		FC.bornChild(f3, "Paul", "Evelyn");

		FC.bornChild(f0, "Obi-Wan", "Skyler");
		FC.bornChild(f3, "Donald", "Courtney");

		System.out.println("------------------ALL DONE SUCCESSFULLY------------------");
	}

	void handleFamilyCreate() {
		while (true) {
			System.out.println("-| INPUT SOME DETAILS ABOUT NEW FAMILY: ");
			System.out.print("---| Father`s name           >>");
			String fName = sc.next();
			System.out.print("---| Father`s surname        >>");
			String fSurname = sc.next();
			System.out.print("---| Father`s year of birth  >>");
			String fYear = sc.next();
			System.out.print("---| Father`s month of birth >>");
			String fMonth = sc.next();
			System.out.print("---| Father`s day of birth   >>");
			String fDay = sc.next();
			System.out.print("---| Father`s iq             >>");
			String fIq = sc.next();


			System.out.print("---| Mother`s name           >>");
			String mName = sc.next();
			System.out.print("---| Mother`s surname        >>");
			String mSurname = sc.next();
			System.out.print("---| Mother`s year of birth  >>");
			String mYear = sc.next();
			System.out.print("---| Mother`s month of birth >>");
			String mMonth = sc.next();
			System.out.print("---| Mother`s day om birth   >>");
			String mDay = sc.next();
			System.out.print("---| Mother`s iq             >>");
			String mIq = sc.next();

			try {
				int parsedFIQ = Integer.parseInt(fIq);
				int parsedMIQ = Integer.parseInt(mIq);
				Man f = new Man(fName, fSurname, String.format("%s/%s/%s", fDay, fMonth, fYear), parsedFIQ);
				Woman m = new Woman(mName, mSurname, String.format("%s/%s/%s", mDay, mMonth, mYear), parsedMIQ);
				Family temp = new Family(m, f);
				System.out.println("-| OK, FAMILY LOOKS LIKE \n" + temp.prettyFormat());

				System.out.print("-| IS IT CORRECT [y/n] >>");
				String answer = sc.next();

				if (Objects.equals(answer, "y")) {
					System.out.println("-| GREAT! |-");
					FC.createNewFamily(m, f);
					break;
				} else if (Objects.equals(answer, "n")) {
					System.out.print("-| REFILL OR EXIT [r/e] >>");
					String answer1 = sc.next();
					if (Objects.equals(answer1, "r")) {

					} else {
						break;
					}
				}

			} catch (Exception ex) {
				System.out.println(ERRORMSG);
				System.out.println(ex);
			}

		}

	}
	void bordAndAdoptHandle(){
		while (true) {
			System.out.print("---| Adopt or Born a child in family by index [a/b/exit to menu(e)]   >> ");
			String inputAction = sc.next();
			if(!Objects.equals(inputAction, "e")){
				System.out.print("--| OK, input a family ID       >> ");
				String inputId = sc.next();
				try {
					int parsedId = Integer.parseInt(inputId);

					if (Objects.equals(inputAction, "b")) {

						System.out.print("--| Boy name                    >> ");
						String iNB = sc.next();
						System.out.print("--| Girl name                   >> ");
						String iNG = sc.next();
						FC.bornChild(FC.getFamilyById(parsedId), iNB, iNG);
						break;

					} else if (Objects.equals(inputAction, "a")) {
						System.out.print("--| Child`s Sex[b/g]            >> ");
						String iSx = sc.next();
						System.out.print("--| Child`s Name                >> ");
						String iN = sc.next();
						System.out.print("--| Child`s Surname             >> ");
						String iS = sc.next();
						System.out.print("--| Child`s year of birth       >> ");
						String iY = sc.next();
						System.out.print("--| Child`s month of birth      >> ");
						String iM = sc.next();
						System.out.print("--| Child`s day of birth        >> ");
						String iD = sc.next();
						System.out.print("--| Child`s iq                  >> ");
						String iIq = sc.next();

						try {
							Human ch0;
							int parsedIq = Integer.parseInt(iIq);
							ch0 = new Man(iN, iS, String.format("%s/%s/%s", iD, iM, iY), parsedIq);
							if (Objects.equals(iSx, "b")) {
								ch0 = new Man(iN, iS, String.format("%s/%s/%s", iD, iM, iY), parsedIq);
							} else if (Objects.equals(iSx, "g")) {
								ch0 = new Woman(iN, iS, String.format("%s/%s/%s", iD, iM, iY), parsedIq);
							}
							System.out.println("-| OK, FAMILY LOOKS LIKE \n" + ch0);
							System.out.print("-| IS IT CORRECT [y/n] >>");
							String answer = sc.next();
							if (Objects.equals(answer, "y")) {
								System.out.println("-| GREAT! |-");
								FC.adoptChild(FC.getFamilyById(parsedId), ch0);
								break;
							} else if (Objects.equals(answer, "n")) {
								System.out.print("-| REFILL OR EXIT [r/e] >>");
								String answer1 = sc.next();
								if (Objects.equals(answer1, "r")) {

								} else {
									break;
								}
							}
						} catch (Exception ex) {
							System.out.println("---| ERROR OCCURRED PLEASE TRY AGAIN");
							System.out.println(ex);
						}
					}
				} catch (Exception ex) {
					System.out.println(ERRORMSG);
					System.out.println(ex);
				}
			} else if (Objects.equals(inputAction, "e")) {
				System.out.println("---| EXITING TO MAIN MENU");
				break;
			}

		}
	}

	void handleInput(int input) {
		switch (input) {
			case (1):
				if (!testDataFilled) {
					fillWithTestData();
					testDataFilled = true;
				} else {
					System.out.println("---YOU ALREADY FILLED UP DB WITH TESTDATA---");
				}
				;
				break;
			case (2):
				FC.displayAllFamiliesPretty();
				break;
			case (3):
				System.out.print("---| enter range >> ");
				String inputRange = sc.next();
				try {
					int parsedRange0 = Integer.parseInt(inputRange);
					List<Family> l0 = FC.getFamiliesBiggerThan(parsedRange0);
					FC.displayInputedPretty(l0);
				} catch (Exception ex) {

					System.out.println(ERRORMSG + ex);
				}
				break;
			case (4):
				System.out.print("---| enter range  >> ");
				String inputRange1 = sc.next();
				try {
					int parsedRange = Integer.parseInt(inputRange1);
					List<Family> l0 = FC.getFamiliesLessThan(parsedRange);
					FC.displayInputedPretty(l0);
				} catch (Exception ex) {

					System.out.println(ERRORMSG + ex);
				}
				break;
			case (5):
				System.out.print("---| enter exact value >> ");
				String inputRange2 = sc.next();
				try {
					int parsedRange = Integer.parseInt(inputRange2);
					int amount = FC.countFamiliesWithMemberNumber(parsedRange);
					if (amount == 0) {
						System.out.println("-| FAMILIES NOT FOUND");
					} else {
						System.out.println("-| FAMILIES FOUND: " + amount);
					}
				} catch (Exception ex) {

					System.out.println(ERRORMSG + ex);
				}
				break;
			case (6):
				handleFamilyCreate();
				break;
			case (7):
				System.out.print("---| enter exact value >> ");
				String inputRange3 = sc.next();
				try {
					int parsedRange = Integer.parseInt(inputRange3);
					FC.deleteFamilyByIndex(parsedRange);
				} catch (Exception ex) {
					System.out.println(ERRORMSG + ex);
				}
				break;
			case (8):
				bordAndAdoptHandle();
				break;
			case(9):
				System.out.print("--| enter exact value >> ");
				String inputRange4 = sc.next();
				try {
					int parsedInput = Integer.parseInt(inputRange4);
					FC.deleteAllChildrenOlderThen(parsedInput);
					System.out.println("--| DONE! |--");
					break;
				}catch (Exception ex){System.out.println(ERRORMSG + ex);
				}
			case(10):
				try {
					FC.save();
				} catch (IOException e) {
					System.out.println(ERRORMSG);
					throw new RuntimeException(e);
				}
				break;
			case(11):
				try {
					FC.load();
				} catch (IOException e) {
					System.out.println(ERRORMSG);
					throw new RuntimeException(e);
				} catch (ClassNotFoundException e) {
					System.out.println(ERRORMSG);
					throw new RuntimeException(e);
				}
				break;

			default:
				System.out.println("----------------------OUT OF BOUNDS---------------------");
		}

	}
	public static void clear() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
	}

}
