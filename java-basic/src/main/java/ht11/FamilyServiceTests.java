package ht11;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

public class FamilyServiceTests {
	CollectionFamilyDao FD = new CollectionFamilyDao();
	FamilyService FS = new FamilyService(FD);
	FamilyController FC = new FamilyController(FS);

	@Test
	void testGetAll(){
		Family f0 = new Family(new Man(), new Woman());
		List<Family> l0 = new ArrayList<>();
		FD.saveFamily(f0);
		l0.add(f0);
		assertEquals(l0,FC.getAllFamilies() );
	}
	@Test
	void testGetBiggerLessEqualsThan(){
		Family f0 = new Family(new Man(), new Woman());
		Family f1 = new Family(new Man(), new Woman());
		List<Family> l0 = new ArrayList<>();
		FD.saveFamily(f0);
		l0.add(f0);
		assertEquals(FC.getFamiliesLessThan(3), l0);
		assertEquals(FC.getFamiliesBiggerThan(1), l0);
		l0.clear();
		assertEquals(FC.getFamiliesLessThan(1), l0);
		assertEquals(FC.getFamiliesBiggerThan(3), l0);
		l0.add(f0);
		assertEquals(FC.countFamiliesWithMemberNumber(2), 1);
	}
	@Test
	void testBornAdopt(){
		Family f0 = new Family(new Man("Walter", "White", System.currentTimeMillis()),new Woman("Jane", "White", System.currentTimeMillis()));
		Woman ch1 = new Woman("Sanna", "Marin", System.currentTimeMillis());
		FC.bornChild(f0, "Jessy", "Margolus");
		FC.adoptChild(f0, ch1);
		assertEquals(FC.getFamilyById(0).getChildren().size(), 2);
		assertEquals(FC.getFamilyById(0).getChildren().get(0).getSurname(), "White");
		assertEquals(FC.getFamilyById(0).getChildren().get(1).getName(), "Sanna");
	}
	@Test
	void testAddDeleteCountGet(){
		FC.createNewFamily( new Woman("Jahne", "Wock", System.currentTimeMillis()),new Man("John", "Wick", System.currentTimeMillis()));
		assertEquals(FC.count(), 1);
		assertEquals(FC.getFamilyById(0).getFather().getName(), "John");
		assertEquals(FC.getFamilyById(0).getFather().getSurname(), "Wick");
		FC.deleteFamilyByIndex(0);
		assertEquals(FC.count(), 0);
	}
}
