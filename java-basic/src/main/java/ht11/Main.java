package ht11;


import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class Main {
	public static void main(String[] args) {
		FamilyManager app = new FamilyManager();
		app.run();
	}

}
enum Species {
	Dog,
	Cat,
	Fish,
	Robocat,
	UNKNOWN
}

enum DayOfWeek {
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Sunday
}
