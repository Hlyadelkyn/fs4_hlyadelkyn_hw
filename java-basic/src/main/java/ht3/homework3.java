package ht3;

import java.io.InputStream;
import java.util.Scanner;

public class homework3 {
    public static String[][] initArr() {
        final String[][] scedule = new String[7][2];
        scedule[0][0] = "Monday";
        scedule[0][1] = "Grocery shopping";
        scedule[1][0] = "Tuesday";
        scedule[1][1] = "Laundry";
        scedule[2][0] = "Wednesday";
        scedule[2][1] = "Vacuuming";
        scedule[3][0] = "Thursday";
        scedule[3][1] = "Pay bills";
        scedule[4][0] = "Friday";
        scedule[4][1] = "Meal planning and prep";
        scedule[5][0] = "Saturday";
        scedule[5][1] = "Cleaning bathrooms";
        scedule[6][0] = "Sunday";
        scedule[6][1] = "Rest and relaxation";
        return scedule;
    }
    public static void main(String[] args) {

        final String[][] schedule = initArr();

        InputStream in = System.in;
        var scanner = new Scanner(in);

        loop: while(true){
            System.out.print("Please, input the day of the week -> ");
            String userInput = scanner.nextLine();
            switch(userInput.toLowerCase().trim()) {
                case ("exit"):
                    System.out.println("exiting");
                    break loop;

                case "monday": //неможливе використання змінної оскільки constant expression required
                    System.out.println(schedule[0][1]);
                    break;

                case "tuesday":
                    System.out.println(schedule[1][1]);
                    break;
                case "wednesday":
                    System.out.println(schedule[2][1]);
                    break;
                case "thursday":
                    System.out.println(schedule[3][1]);
                    break;
                case "friday":
                    System.out.println(schedule[4][1]);
                    break;
                case "saturday":
                    System.out.println(schedule[5][1]);
                    break;
                case "sunday":
                    System.out.println(schedule[6][1]);
                    break;
                default:
                    System.out.println("Sorry, I don\'t understand you, please try again");
                    break;
            }
        }

    }
}
