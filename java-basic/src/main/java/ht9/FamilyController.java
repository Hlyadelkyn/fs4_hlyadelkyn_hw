package ht9;

import java.util.List;
import java.util.Set;

public class FamilyController {
	FamilyService familyService;

	FamilyController(FamilyService a) {
		this.familyService = a;
	}

	List<Family> getAllFamilies() {
		return familyService.getAllFamilies();
	}

	void displayALlFamilies() {
		familyService.displayAllFamilies();
	}

	List<Family> getFamiliesBiggerThan(int inputindex) {
		return familyService.getFamiliesBiggerThan(inputindex);
	}

	List<Family> getFamiliesLessThan(int inputIndex) {
		return familyService.getFamiliesLessThan(inputIndex);
	}

	int countFamiliesWithMemberNumber(int inputNum) {
		return familyService.countFamiliesWithMemberNumber(inputNum);
	}

	void createNewFamily(Human h0, Human h1) {
		familyService.createNewFamily(h0, h1);
	}

	void deleteFamilyByIndex(int inputIndex) {
		familyService.deleteFamilyByIndex(inputIndex);
	}

	void bornChild(Family f0, String name, String nameW) {
		familyService.bornChild(f0, name, nameW);
	}

	void adoptChild(Family f0, Human ch0) {
		familyService.adoptChild(f0, ch0);
	}

	void deleteAllChildrenOlderThen(int inputIndex) {
		familyService.deleteAllChildrenOlderThen(inputIndex);
	}

	int count() {
		return familyService.count();
	}

	Family getFamilyById(int id) {
		return familyService.getFamilyById(id);
	}

	Set<Pet> getPets(int id) {
		return familyService.getPets(id);
	}

	void addPet(int id, Pet p0) {
		familyService.addPet(id, p0);
	}


}
