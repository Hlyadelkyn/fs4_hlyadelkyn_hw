package ht9;


import java.time.Instant;
import java.util.Date;

enum Species {
	Dog,
	Cat,
	Fish,
	Robocat,
	UNKNOWN
}

enum DayOfWeek {
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Sunday
}

public class Main {


	public static void main(String[] args) {
		Man h0 = new Man();
		h0.setBirthdate(1049835126000L);
        System.out.println(h0);
		System.out.println(new Date(1049835126000L));
	}


}
