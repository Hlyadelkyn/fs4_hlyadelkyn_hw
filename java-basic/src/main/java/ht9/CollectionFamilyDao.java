package ht9;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

interface FamilyDao {
	List<Family> getAllFamilies();

	Family getByFamilyIndex(int inputIndex);

	boolean deleteFamily(int inputIndex);

	boolean deleteFamily(Family inputFamily);

	void saveFamily(Family inputFamily);
}

public class CollectionFamilyDao implements FamilyDao {
	List<Family> dataBase = new ArrayList<>();

	public List<Family> getAllFamilies() {
		return this.dataBase;
	}

	public Family getByFamilyIndex(int inputIndex) {
		return this.dataBase.get(inputIndex);
	}

	public boolean deleteFamily(int inputIndex) {
		try {
			this.dataBase.remove(inputIndex);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public boolean deleteFamily(Family inputFamily) {
		try {
			this.dataBase.remove(inputFamily);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public void saveFamily(Family inputFamily) {
		boolean familyExist = false;

		for(int i = 1; i < this.dataBase.size(); i++ ) {
			if (this.dataBase.get(i).getFather()==inputFamily.getFather() && this.dataBase.get(i).getMother() == inputFamily.getMother()) {
				familyExist =true;
				this.dataBase.set(i, inputFamily);
			}
		}
		if (!familyExist){
			this.dataBase.add(inputFamily);
		}
	}
}
