package ht5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

import java.util.Arrays;

public class ClassTests {


	@Test
	public void TestString() {
		System.out.println("Testing Human toString method");
		assertEquals("Human{name='Jane', surname='Margolis', year=1991,  schedule - null", new Human("Jane", "Margolis", 1991).toString());
		assertEquals("Human{name='null', surname='null', year=0,  schedule - null", new Human().toString());
	}

	@Test
	public void TestDelChildInt() {
		Family f0 = new Family(new Human(), new Human());
		f0.addChild(new Human());
		f0.deleteChild(2);
		assertEquals(1, f0.getChildren().length);

		f0.deleteChild(0);
		assertEquals(0, f0.getChildren().length);
	}

	@Test
	public void TestDelChildHuman(){
		Family f0 = new Family(new Human(),new Human());
		Human ch0 = new Human("Walter", "White", 1990);
		Human ch1 = new Human("Jessy", "Pinkman", 1902);
		f0.addChild(ch0);
		assertEquals(1 , f0.getChildren().length);
		f0.deleteChild(ch1);
		assertEquals(1 , f0.getChildren().length);
		f0.deleteChild(ch0);
		assertEquals(0, f0.getChildren().length);

	}
	@Test
	public void TestAddChild() {
		Family f0 = new Family(new Human(), new Human());
		Family f1 = new Family(new Human(), new Human());
		f0.addChild(new Human("Jessy", "Pinkman", 2000));
		f1.addChild(new Human("Jessy", "Pinkman", 2000));

		f1.addChild(new Human("Walter", "white", 1999));
		System.out.println("Testing addChild method");
		assertEquals(f0.getChildren().length, f1.getChildren().length - 1);
		assertEquals(f1.getChildren()[f1.getChildren().length - 1], new Human("Walter", "white", 1999));

	}

	@Test
	public void TestCountFamily() {
		Family f0 = new Family(new Human(), new Human());
		f0.addChild(new Human());
		System.out.println("Testing countFamily method");
		assertEquals(2, new Family(new Human(), new Human()).countFamily());
		assertEquals(3, f0.countFamily());
	}
}
