package ht5;

import java.util.Arrays;

public class Family {
	private Human mother;
	private Human father;
	private Human[] children;

	Family(Human inputMother, Human inputFather) {
		inputMother.setFamily(this);
		inputFather.setFamily(this);
		mother = inputMother;
		father = inputFather;
	}

	int countFamily() {
		if(!(children==null))return 2 + children.length;
		return 2;
	}

	void addChild(Human childToAdd) {
		childToAdd.family = father.family;
		Human[] tempArr;
		try{
			tempArr = Arrays.copyOf(this.children, this.children.length + 1);
		} catch (Exception ex){
			tempArr = new Human[1];
		}

		tempArr[tempArr.length - 1] = childToAdd;
		this.children = tempArr;
	}

	boolean deleteChild(int deleteIndex) {
		if (!(this.children == null || deleteIndex < 0
				|| deleteIndex >= this.children.length)) {

			Human[] tempArr = new Human[this.children.length - 1];
			for (int i = 0, k = 0; i < this.children.length; i++) {
				if (i == deleteIndex) {
					this.children[i].family = null;
					continue;
				}
				tempArr[k++] = this.children[i];
			}
			this.children = tempArr;
			return true;

		} else {
			return false;
		}

	}
	boolean deleteChild (Human childToDelete){
		try {
			int index = Arrays.asList(children).indexOf(childToDelete);
			Human[] tempArr = new Human[this.children.length - 1];
			for (int i = 0, k = 0; i < this.children.length; i++) {
				if (i == index) {
					this.children[i].family = null;
					continue;
				}
				tempArr[k++] = this.children[i];
			}
			this.children = tempArr;
			return true;
		}catch (Exception ex){
			System.out.println(ex);
			return false;
		}

	}

	@Override
	public String toString() {
		return "Mother - " + mother.toString() + ", father - " + father.toString() + " children - " + Arrays.toString(this.children) + "";
	}

	@Override
	public boolean equals(Object inputObj) {
		if (!(inputObj instanceof Family)) return false;
		return ((Family) inputObj).getFather().equals(this.father) && ((Family) inputObj).getMother().equals(this.mother) && Arrays.equals(((Family) inputObj).getChildren(), this.children);
	}

	public Human getMother() {
		return mother;
	}

	public void setMother(Human mother) {
		this.mother = mother;
	}

	public Human getFather() {
		return father;
	}

	public void setFather(Human father) {
		this.father = father;
	}

	public Human[] getChildren() {
		return children;
	}

	public void setChildren(Human[] children) {
		this.children = children;
	}

	protected void finalize() throws Throwable {
		try {
			System.out.println("Family " + this.toString() + " deleted");
		} catch (Throwable e) {
			throw e;
		} finally {
//			System.out.println("inside Human class finalize()");
			super.finalize();
		}
	}
}
