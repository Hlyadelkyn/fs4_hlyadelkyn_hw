package ht5;

import java.util.Arrays;

public class Pet {
	private Species species;
	private String nickname;
	private int age;
	private int trickLevel;
	private String[] habits;

	Pet() {}

	Pet(Species inputSpecies, String inputNickname) {
		species = inputSpecies;
		nickname = inputNickname;
	}

	Pet(Species inputSpecies, String inputNickname, int inputAge, int inputTrickLevel, String[] inputHabits) {
		if (inputTrickLevel > 100 || inputTrickLevel < 0) throw new IllegalArgumentException("Trick level should be in the range of [0-100].");
		species = inputSpecies;
		nickname = inputNickname;
		age = inputAge;
		trickLevel = inputTrickLevel;
		habits = inputHabits;
	}


	void eat() {System.out.println("I`m eating!");}

	void respond() {
		System.out.println(String.format("Hi master! It`s me, %s, I missed you", nickname));
	}

	void foul() {System.out.println("there is need to cover your tracks well");}

	@Override
	public String toString() {
		return String.format("%s{nickname='%s', age=%s, trick level=%s, habits=%s", species.name(),nickname, age, trickLevel, Arrays.toString(habits));
	}

	@Override
	public boolean equals(Object inputObj){
		if(!(inputObj instanceof Pet)) return false;
		if(inputObj instanceof Pet && ((Pet) inputObj).getAge() == this.age && ((Pet) inputObj).getNickname() == this.nickname && ((Pet) inputObj).getSpecies() == this.species ) return true;
		return false;
	}
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public int getTrickLevel() {
		return trickLevel;
	}

	public void setTrickLevel(int trickLevel) {if(trickLevel < 100 && trickLevel>0)this.trickLevel = trickLevel;}

	public Species getSpecies() {
		return species;
	}

	public void setSpecies(Species species) {
		this.species = species;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String[] getHabits() {
		return habits;
	}

	public void setHabits(String[] habits) {
		this.habits = habits;
	}

	protected void finalize() throws Throwable {
		try {
			System.out.println("Pet" + this.toString() + " deleted");
		} catch (Throwable e) {
			throw e;
		} finally {
//			System.out.println("inside Human class finalize()");
			super.finalize();
		}
	}
}
