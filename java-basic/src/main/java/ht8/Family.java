package ht8;

import java.util.*;

public class Family {
	private Human mother;
	private Human father;
	private ArrayList<Human> children = new ArrayList<Human>();

	private Set<Pet> pet;

	Family(Human inputMother, Human inputFather) {
		inputMother.setFamily(this);
		inputFather.setFamily(this);
		mother = inputMother;
		father = inputFather;
	}

	int countFamily() {
		if (!(children == null)) return 2 + children.size();
		return 2;
	}

	void addChild(Human childToAdd) {
		this.children.add(childToAdd);
	}

	boolean deleteChild(int deleteIndex) {
		if (!(this.children == null || deleteIndex < 0
				|| deleteIndex >= this.children.size())) {
			this.children.remove(deleteIndex);
			return true;

		} else {
			return false;
		}

	}

	boolean deleteChild(Human childToDelete) {
		try {
			this.children.remove(childToDelete);
			return true;
		} catch (Exception ex) {
			System.out.println(ex);
			return false;
		}

	}

	@Override
	public String toString() {
		return "Mother - " + mother.toString() + ", father - " + father.toString() + " children - " + this.children + "";
	}

	@Override
	public boolean equals(Object inputObj) {
		if (!(inputObj instanceof Family)) return false;
		return (((Family) inputObj).getFather().equals(this.father) && ((Family) inputObj).getMother().equals(this.mother) && ((Family) inputObj).getChildren().equals(this.children));
	}

	public Human getMother() {
		return mother;
	}

	public void setMother(Human mother) {
		this.mother = mother;
	}

	public Human getFather() {
		return father;
	}

	public void setFather(Human father) {
		this.father = father;
	}

	public List<Human> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<Human> children) {
		this.children = children;
	}

	public Set<Pet> getPet() {
		return pet;
	}

	public void setPet(Set<Pet> pet) {
		this.pet = pet;
	}

	protected void finalize() throws Throwable {
		try {
			System.out.println("Family " + this.toString() + " deleted");
		} catch (Throwable e) {
			throw e;
		} finally {
//			System.out.println("inside Human class finalize()");
			super.finalize();
		}
	}
}
