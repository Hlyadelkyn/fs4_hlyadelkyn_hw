package ht8;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

abstract class Human {
	Family family;
	private String name;
	private String surname;
	private int iq;
	private long birthdate;

	private Pet pet;


	private Map<DayOfWeek, String> schedule;

	Human() {}

	Human(String inputName, String inputSurname, long inputYear) {
		name = inputName;
		surname = inputSurname;
		birthdate = inputYear;
	}

	Human(String inputName, String inputSurname, long inputYear, Human inputMother, Human inputFather) {
		name = inputName;
		surname = inputSurname;
		birthdate = inputYear;
	}

	Human(String inputName, String inputSurname, long inputYear, int inputIq, Human inputMother, Human inputFather, Map<DayOfWeek, String> inputSchedule) {
		if (inputIq > 100 || inputIq < 0) throw new IllegalArgumentException("IQ should be in the range of [0-100].");
		name = inputName;
		surname = inputSurname;
		birthdate = inputYear;
		iq = inputIq;
		schedule = inputSchedule;
	}

	Human(String inputName, String inputSurname, String inputBD, int inputIq) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date;
		try {
			date = simpleDateFormat.parse(inputBD);
			System.out.println("Date : " + date);
			birthdate = date.getTime();
			name = inputName;
			surname = inputSurname;
			iq = inputIq;
		} catch (ParseException e) {e.printStackTrace();}

	}


	void describeAge() {
		Period age = Period.between(Instant.ofEpochMilli(this.birthdate).atZone(ZoneId.systemDefault()).toLocalDate(), LocalDate.now());
		System.out.println(String.format("Human is %s old, %s month and %s days", age.getYears(), age.getMonths(), age.getDays()));
	}

	void describePet() {
		try {
			String tempPhrase;
			if (this.pet.getTrickLevel() > 50) tempPhrase = "very tricky";
			else tempPhrase = "almost not tricky";
			System.out.println(String.format("I have %s, it is %s years old, it is %s", this.pet.getSpecies(), this.pet.getAge(), tempPhrase));
		} catch (Exception e) {
			throw new RuntimeException("no family or pet in");
		}

	}

	@Override
	public String toString() {
		Date ageDate = new Date(this.birthdate);

		if (this.pet != null) {
			return String.format("Human{name='%s', surname='%s', birth= %s/%s/%s, ", name, surname, ageDate.getDate(), ageDate.getMonth() + 1, ageDate.getYear() + 1900) +
					" " +
					"schedule " +
					"- " + this.schedule + " pet - " + pet.toString();

		}
		return String.format("Human{name='%s', surname='%s', birth= %s/%s/%s, ", name, surname, ageDate.getDate(), ageDate.getMonth() + 1, ageDate.getYear() + 1900) + " " +
				"schedule - " + this.schedule;
	}

	@Override
	public boolean equals(Object inputObj) {
		if (!(inputObj instanceof Human)) return false;
		return ((Human) inputObj).getIq() == this.iq && ((Human) inputObj).getBirthdate() == this.birthdate && ((Human) inputObj).getSchedule() == this.schedule;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {return surname;}

	public void setSurname(String surname) {this.surname = surname;}

	public Map<DayOfWeek, String> getSchedule() {return schedule;}

	public void setSchedule(Map<DayOfWeek, String> schedule) {this.schedule = schedule;}


	public int getIq() {return iq;}

	public void setIq(int iq) {if (iq > 0 && iq < 100) this.iq = iq;}


	public Pet getPet() {
		return pet;
	}

	public void setPet(Pet pet) {
		this.pet = pet;
	}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	public long getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(long birthdate) {
		this.birthdate = birthdate;
	}


	protected void finalize() throws Throwable {
		try {

		} catch (Throwable e) {
			throw e;
		} finally {
//			System.out.println("inside Human class finalize()");
			super.finalize();
		}
	}
}

final class Man extends Human {

	Man() {
		super();
	}

	Man(String inputName, String inputSurname, long inputYear) {
		super(inputName, inputSurname, inputYear);
	}

	Man(String inputName, String inputSurname, long inputYear, Human inputMother, Human inputFather) {
		super(inputName, inputSurname, inputYear, inputMother, inputFather);
	}

	Man(String inputName, String inputSurname, long inputYear, int inputIq, Human inputMother, Human inputFather, Map<DayOfWeek, String> inputSchedule) {
		super(inputName, inputSurname, inputYear, inputIq, inputMother, inputFather, inputSchedule);
	}
	Man(String inputName, String inputSurname, String inputBD, int inputIq) {
		super(inputName, inputSurname, inputBD, inputIq);
	}

	void greetPet() {
		try {System.out.println(String.format("wassup, %s", super.getPet().getNickname()));} catch (Exception e) {
			throw new RuntimeException("no family or pet in")
					;
		}
	}

	void repairCar() {
		System.out.println("lightweight baby");
	}

}

final class Woman extends Human {
	Woman() {
		super();
	}

	Woman(String inputName, String inputSurname, long inputYear) {
		super(inputName, inputSurname, inputYear);
	}

	Woman(String inputName, String inputSurname, long inputYear, Human inputMother, Human inputFather) {
		super(inputName, inputSurname, inputYear, inputMother, inputFather);
	}

	Woman(String inputName, String inputSurname, long inputYear, int inputIq, Human inputMother, Human inputFather, Map<DayOfWeek, String> inputSchedule) {
		super(inputName, inputSurname, inputYear, inputIq, inputMother, inputFather, inputSchedule);
	}
	Woman(String inputName, String inputSurname, String inputBD, int inputIq) {
		super(inputName, inputSurname, inputBD, inputIq);
	}


	void greetPet() {
		try {System.out.println(String.format("Hiiiii^^, %s", super.getPet().getNickname()));} catch (Exception e) {
			throw new RuntimeException("no family or pet in")
					;
		}
	}

	void makeup() {
		System.out.println("making up the most beautiful vumen in the world");
	}
}

