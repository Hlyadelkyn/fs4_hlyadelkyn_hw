package ht7;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

import java.util.Arrays;

public class ClassTests {


	@Test
	public void TestString() {
		System.out.println("Testing Human toString method");
		assertEquals("Human{name='Jane', surname='Margolis', year=1991,  schedule - null", new Man("Jane", "Margolis", 1991).toString());
		assertEquals("Human{name='null', surname='null', year=0,  schedule - null", new Man().toString());
	}

	@Test
	public void TestDelChildInt() {
		Family f0 = new Family(new Man(), new Woman());
		f0.addChild(new Man());
		f0.deleteChild(2);
		assertEquals(1, f0.getChildren().size());

		f0.deleteChild(0);
		assertEquals(0, f0.getChildren().size());
	}

	@Test
	public void TestDelChildHuman(){
		Family f0 = new Family(new Man(),new Woman());
		Human ch0 = new Man("Walter", "White", 1990);
		Human ch1 = new Man("Jessy", "Pinkman", 1902);
		f0.addChild(ch0);
		assertEquals(1 , f0.getChildren().size());
		f0.deleteChild(ch1);
		assertEquals(1 , f0.getChildren().size());
		f0.deleteChild(ch0);
		assertEquals(0, f0.getChildren().size());

	}
	@Test
	public void TestAddChild() {
		Family f0 = new Family(new Man(), new Woman());
		Family f1 = new Family(new Man(), new Woman());
		f0.addChild(new Man("Jessy", "Pinkman", 2000));
		f1.addChild(new Man("Jessy", "Pinkman", 2000));

		f1.addChild(new Man("Walter", "white", 1999));
		System.out.println("Testing addChild method");
		assertEquals(f0.getChildren().size(), f1.getChildren().size() - 1);
		assertEquals(f1.getChildren().get(f1.getChildren().size() - 1), new Man("Walter", "white", 1999));

	}

	@Test
	public void TestCountFamily() {
		Family f0 = new Family(new Man(), new Woman());
		f0.addChild(new Man());
		System.out.println("Testing countFamily method");
		assertEquals(2, new Family(new Man(), new Woman()).countFamily());
		assertEquals(3, f0.countFamily());
	}
}
