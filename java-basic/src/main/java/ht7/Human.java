package ht7;

import java.util.Arrays;
import java.util.Map;

abstract class Human {
	Family family;
	private String name;
	private String surname;
	private int year;
	private int iq;

	private Pet pet;


	private Map<DayOfWeek, String> schedule;

	Human() {}

	Human(String inputName, String inputSurname, int inputYear) {
		name = inputName;
		surname = inputSurname;
		year = inputYear;
	}

	Human(String inputName, String inputSurname, int inputYear, Human inputMother, Human inputFather) {
		name = inputName;
		surname = inputSurname;
		year = inputYear;
	}

	Human(String inputName, String inputSurname, int inputYear, int inputIq, Human inputMother, Human inputFather, Map<DayOfWeek, String> inputSchedule) {
		if (inputIq > 100 || inputIq < 0) throw new IllegalArgumentException("IQ should be in the range of [0-100].");
		name = inputName;
		surname = inputSurname;
		year = inputYear;
		iq = inputIq;
		schedule = inputSchedule;
	}




	void describePet() {
		try {
			String tempPhrase;
			if (this.pet.getTrickLevel() > 50) tempPhrase = "very tricky";
			else tempPhrase = "almost not tricky";
			System.out.println(String.format("I have %s, it is %s years old, it is %s", this.pet.getSpecies(), this.pet.getAge(), tempPhrase));
		} catch (Exception e) {
			throw new RuntimeException("no family or pet in");
		}

	}

	@Override
	public String toString() {
		if(this.pet != null){
			return String.format("Human{name='%s', surname='%s', year=%s, ", name, surname, year) + " schedule - " + this.schedule + " pet - " + pet.toString();

		}
		return String.format("Human{name='%s', surname='%s', year=%s, ", name, surname, year) + " schedule - " + this.schedule;
	}

	@Override
	public boolean equals(Object inputObj) {
		if (!(inputObj instanceof Human)) return false;
		return ((Human) inputObj).getIq() == this.iq && ((Human) inputObj).getYear() == this.year && ((Human) inputObj).getSchedule() == this.schedule;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {return surname;}

	public void setSurname(String surname) {this.surname = surname;}

	public Map<DayOfWeek, String> getSchedule() {return schedule;}

	public void setSchedule(Map<DayOfWeek, String> schedule) {this.schedule = schedule;}


	public int getIq() {return iq;}

	public void setIq(int iq) {if (iq > 0 && iq < 100) this.iq = iq;}

	public int getYear() {return year;}

	public void setYear(int year) {this.year = year;}

	public void setPet(Pet pet) {
		this.pet = pet;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	public Pet getPet() {
		return pet;
	}

	public Family getFamily() {
		return family;
	}


	protected void finalize() throws Throwable {
		try {

		} catch (Throwable e) {
			throw e;
		} finally {
//			System.out.println("inside Human class finalize()");
			super.finalize();
		}
	}
}
final class Man extends Human {

	Man(){
		super();
	}
	Man(String inputName, String inputSurname, int inputYear) {
		super(inputName, inputSurname, inputYear);
	}
	Man(String inputName, String inputSurname, int inputYear, Human inputMother, Human inputFather){
		super(inputName, inputSurname , inputYear , inputMother , inputFather);
	}
	Man(String inputName, String inputSurname, int inputYear, int inputIq, Human inputMother, Human inputFather, Map<DayOfWeek, String> inputSchedule){
		super(inputName, inputSurname, inputYear, inputIq, inputMother, inputFather , inputSchedule);
	}
	void greetPet() {
		try {System.out.println(String.format("wassup, %s", super.getPet().getNickname()));} catch (Exception e) {
			throw new RuntimeException("no family or pet in")
					;
		}
	}
	void repairCar (){
		System.out.println("lightweight baby");
	}

}
final class Woman extends Human{
	Woman(){
		super();
	}
	Woman(String inputName, String inputSurname, int inputYear) {
		super(inputName, inputSurname, inputYear);
	}
	Woman(String inputName, String inputSurname, int inputYear, Human inputMother, Human inputFather){
		super(inputName, inputSurname , inputYear , inputMother , inputFather);
	}
	Woman(String inputName, String inputSurname, int inputYear, int inputIq, Human inputMother, Human inputFather, Map<DayOfWeek, String> inputSchedule){
		super(inputName, inputSurname, inputYear, inputIq, inputMother, inputFather , inputSchedule);
	}


	void greetPet() {
		try {System.out.println(String.format("Hiiiii^^, %s", super.getPet().getNickname()));} catch (Exception e) {
			throw new RuntimeException("no family or pet in")
					;
		}
	}
	void  makeup (){
		System.out.println("making up the most beautiful vumen in the world");
	}
}

